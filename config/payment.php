<?php 
return [
    'transaction_fee' => [
        'percent' => env('TRANSACTION_PERCENT', 3),
        'max' => env('TRANSACTION_MAX', 2000)
    ]
];