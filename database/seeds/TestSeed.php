<?php

use Illuminate\Database\Seeder;

class TestSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User', 5)->create()->each(function ($user) {
            factory('App\Media', 5)->create([
                'user_id' => $user->id
            ])->each(function ($media) {
                factory('App\Program', 5)->create(['media_id' => $media->id])
                    ->each(function ($program) {
                        factory('App\Slot', 5)->create(['program_id' => $program->id]);
                    });
            });
        });
    }
}
