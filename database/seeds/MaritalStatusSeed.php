<?php

use App\MaritalStatus;
use Illuminate\Database\Seeder;

class MaritalStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect([
            'single', 'divorce', 'married', 'widow', 'widower'
        ])->each(function (string $status) {
            MaritalStatus::firstOrCreate([
                'name' => $status
            ]);
        });
    }
}
