<?php

use Illuminate\Database\Seeder;

class UserAccountTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ["advertiser", 'publisher', 'administrator', 'affiliate'];

        foreach ($types as $type) {
            \App\AccountType::firstOrCreate([
                'name' => $type
            ]);
        }
    }
}
