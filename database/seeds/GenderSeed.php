<?php

use Illuminate\Database\Seeder;

class GenderSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(['males', 'females', 'gays', 'lesbians', 'straights', 'homosexuals'])
            ->each(function (string $gender) {
                \App\Gender::firstOrCreate(['name' => $gender]);
            });
    }
}
