<?php

use Illuminate\Database\Seeder;

class AudienceInterestSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $interests = [
            "Fashion",
            "Celebrity, Lifestyle and events",
            "Politics and Governance",
            "Business and Entrepreneurship",
            "Technology",
            "Luxury and Exotic living",
            "Sports",
            "Real Estate",
            "Agriculture",
            "Environment and Green",
            "Oil and Gas",
            "Energy and Power",
            "Media, and Entertainment",
            "Marketing, PR, Advertising",
            "Christianity",
            "Islam",
            "Culture and tradition",
            "Education",
            "Career and Professional",
            "Health, Medicine and Drugs",
            "Law",
            "Finance, Investments and Tax",
            "Family, home",
            "Catering, food, hospitality",
            "Automobiles",
            "Wealth and financial freedom",
            "Start-ups and SMEs",
            "Social change, charity and welfare"
        ];

        foreach ($interests as $interest) {
            \App\Interest::firstOrCreate([
                'name' => $interest
            ]);
        }
    }
}
