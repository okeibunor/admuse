<?php

use Illuminate\Database\Seeder;

class MediaTypeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ["text", 'image', 'video', 'audio', 'other'];

        foreach ($types as $type) {
            \App\MediaType::firstOrCreate([
                'name' => $type
            ]);
        }
    }
}
