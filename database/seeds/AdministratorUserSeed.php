<?php

use App\AccountType;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Database\Seeder;

class AdministratorUserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            User::firstOrCreate([
                'email' => env('APP_MAIL', 'admin@admuse.com'),
                'name' => env('APP_NAME', 'Admuse'),
                'password' => bcrypt('secret'),
                'account_type_id' => AccountType::admin()->id
            ]);
        } catch (QueryException $e) {

        }
    }
}
