<?php

use Illuminate\Database\Seeder;

class AgeGroupSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $groups = [
            "Children 3-13",
            "Teen 13 - 16",
            "Youth 17 - 25" .
            "Youth 26 - 33",
            "Youth 34 - 45",
            "Elderly 46 - 55",
            "Elderly 56 - 70",
            "Elderly 70+"
        ];

        foreach ($groups as $group) {
            \App\AgeGroup::firstOrCreate([
                'name' => $group
            ]);
        }
    }
}
