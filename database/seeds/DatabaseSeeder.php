<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserAccountTypeSeed::class);
        $this->call(AdministratorUserSeed::class);
        $this->call(ChannelSeed::class);
        $this->call(ScheduleSeed::class);
        $this->call(SocialClassSeed::class);
        $this->call(LiteracyLevelSeed::class);
        $this->call(AudienceInterestSeed::class);
        $this->call(AgeGroupSeed::class);
        $this->call(ProfessionLevelSeed::class);
        $this->call(CostFactorSeed::class);
        $this->call(MediaTypeSeed::class);
        $this->call(SlotSizeSeed::class);
        $this->call(GenderSeed::class);
        $this->call(MaritalStatusSeed::class);
    }
}
