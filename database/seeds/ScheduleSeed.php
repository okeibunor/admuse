<?php

use Illuminate\Database\Seeder;

class ScheduleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $schedules = ["Mondays", "Tuesdays", "Wednesdays", 'Thursdays', 'Fridays', 'Saturdays', 'Sundays', 'Daily', 'Weekly', 'Monthly', 'Quarterly', 'Bi Annually', "Yearly"];

        foreach ($schedules as $schedule) {
            \App\Schedule::firstOrCreate([
                'name' => $schedule
            ]);
        }
    }
}
