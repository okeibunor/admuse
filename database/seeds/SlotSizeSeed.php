<?php

use Illuminate\Database\Seeder;

class SlotSizeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(['seconds'])
        ->each(function($value) {
            \App\Size::firstOrCreate(['size' => $value]);
        });
    }
}
