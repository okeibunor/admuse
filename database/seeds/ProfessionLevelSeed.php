<?php

use Illuminate\Database\Seeder;

class ProfessionLevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            "Students",
            "Entry Level Professionals",
            "Mid-level professionals",
            "Mid-senior level professionals",
            "Senior Level Professionals",
            "Retirees",
            "Unskilled workers (Artisans)"
        ];

        foreach ($levels as $level) {
            \App\ProfessionLevel::firstOrCreate([
                'name' => $level
            ]);
        }
    }
}
