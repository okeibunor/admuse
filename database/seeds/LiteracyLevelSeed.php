<?php

use Illuminate\Database\Seeder;

class LiteracyLevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = ["Illiterate","Semi-Literate","Literate", "Highly Literate"];

        foreach ($levels as $level) {
            \App\LiteracyLevel::firstOrCreate([
                'name' => $level
            ]);
        }
    }
}
