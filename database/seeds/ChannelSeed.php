<?php

use Illuminate\Database\Seeder;

class ChannelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $subChannels = [
            'blog' => ['online', 'mobile'],
            'Social Media' => ['online', 'mobile', 'influencer'],
            'Email Marketing' => ['online', 'mobile', 'influencer'],
            'newspaper' => ['print'],
            'magazine' => ['print'],
            'billboard' => ['out of home'],
            'poster' => ['out of home'],
            'cinema' => ['out of home'],
            'mobile' => ['billboard', 'out of home'],
            'online newspaper' => ['online', 'mobile'],
            'online radio' => ['online', 'radio', 'mobile'],
            'In - App Ads' => ['online', 'mobile'],
            'Bus Ads' => ['transit', 'out of home'],
            'Keke Ads' => ['transit', 'out of home'],
            'SMS' => ['mobile'],
            'OAPs' => ['influencer'],
            'Celebrities' => ['influencer'],
            'IG influencer' => ['influencer'],
            'Subject matter experts' => ['influencer'],
            'Large group admins' => ['influencer'],
            'Twitter' => ['influencer', 'influencer'],
            'Snapchat' => ['influencer', 'influencer'],
            'Facebook' => ['influencer', 'influencer'],
            'LinkedIn' => ['influencer', 'influencer'],
            'YouTube' => ['influencer', 'influencer'],
            'Event organizers' => ['influencer'],
            'Events' => ['out of home']
        ];


        foreach ($subChannels as $subChannel => $channels) {
            foreach ($channels as $channel) {
                \App\Channel::firstOrCreate([
                    'name' => $channel
                ])->subChannel()->firstOrCreate([
                    'name' => $subChannel
                ]);
            }
        }
    }
}
