<?php

use Illuminate\Database\Seeder;

class CostFactorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $factors = [
            "Per Publication",
            "Per Post",
            "Per Month",
            "Per Quarter",
            "Per Annum",
            "Per day",
            "Per Week",
            "Per Design",
            "Per Impression",
            "per tour",
            "per word",
            "per second"
        ];

        foreach ($factors as $factor) {
            \App\CostFactor::firstOrCreate([
                'name' => $factor
            ]);
        }
    }
}
