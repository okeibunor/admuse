<?php

use Illuminate\Database\Seeder;

class SocialClassSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = ["Lower Class", "Middle Class", "Upper Class", "High Networth Individuals"];

        foreach ($classes as $class) {
            \App\SocialClass::firstOrCreate([
                'name' => $class
            ]);
        }
    }
}
