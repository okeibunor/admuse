<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFieldFromProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programs', function (Blueprint $table) {
            $foreignKeys = [
                'programs_age_group_id_foreign',
                'programs_interest_id_foreign',
                'programs_literacy_level_id_foreign',
                'programs_profession_level_id_foreign',
                'programs_social_class_id_foreign'
            ];
            collect($foreignKeys)->each(function($key) use ($table) {
                $table->dropForeign($key);
            });
            $table->dropColumn([
                'social_class_id',
                'literacy_level_id',
                'interest_id',
                'age_group_id',
                'profession_level_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programs', function (Blueprint $table) {
            $table->unsignedBigInteger('social_class_id');
            $table->foreign('social_class_id')->references('id')
                ->on('social_classes')->onDelete('cascade');

            $table->unsignedBigInteger('literacy_level_id');
            $table->foreign('literacy_level_id')->references('id')
                ->on('literacy_levels')->onDelete('cascade');

            $table->unsignedBigInteger('interest_id');
            $table->foreign('interest_id')->references('id')
                ->on('interests')->onDelete('cascade');

            $table->unsignedBigInteger('age_group_id');
            $table->foreign('age_group_id')->references('id')
                ->on('age_groups')->onDelete('cascade');

            $table->unsignedBigInteger('profession_level_id');
            $table->foreign('profession_level_id')->references('id')
                ->on('profession_levels')->onDelete('cascade');
        });
    }
}
