<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 190)->index();
            $table->string('short_name', 190)->index()->unique();
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->unsignedBigInteger('reach_min')->default(0);
            $table->unsignedBigInteger('reach_max')->default(1);

            $table->unsignedBigInteger('channel_id');
            $table->foreign('channel_id')->references('id')
                ->on('channels')->onDelete('cascade');

            $table->unsignedBigInteger('sub_channel_id');
            $table->foreign('sub_channel_id')->references('id')
                ->on('sub_channels')->onDelete('cascade');

            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')
                ->on('types')->onDelete('cascade');

            $table->unsignedBigInteger('media_id');
            $table->foreign('media_id')->references('id')
                ->on('media')->onDelete('cascade');

            $table->unsignedBigInteger('schedule_id');
            $table->foreign('schedule_id')->references('id')
                ->on('schedules')->onDelete('cascade');

            $table->unsignedBigInteger('social_class_id');
            $table->foreign('social_class_id')->references('id')
                ->on('social_classes')->onDelete('cascade');

            $table->unsignedBigInteger('literacy_level_id');
            $table->foreign('literacy_level_id')->references('id')
                ->on('literacy_levels')->onDelete('cascade');

            $table->unsignedBigInteger('interest_id');
            $table->foreign('interest_id')->references('id')
                ->on('interests')->onDelete('cascade');

            $table->unsignedBigInteger('age_group_id');
            $table->foreign('age_group_id')->references('id')
                ->on('age_groups')->onDelete('cascade');

            $table->unsignedBigInteger('profession_level_id');
            $table->foreign('profession_level_id')->references('id')
                ->on('profession_levels')->onDelete('cascade');

            $table->unsignedBigInteger('cost_factor_id');
            $table->foreign('cost_factor_id')->references('id')
                ->on('cost_factors')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
