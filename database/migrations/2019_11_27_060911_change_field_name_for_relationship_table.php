<?php

use App\Program;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldNameForRelationshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('relation_ship_paths', function (Blueprint $table) {
            $table->dropForeign('relation_ship_paths_program_id_foreign');
            $table->renameColumn('program_id', 'parent_id');
            $table->string('parent_type', 170)->default('App\\\Program')->index('relation_parent_type_index');
            $table->index('parent_id', 'relation_parent_id_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relation_ship_paths', function (Blueprint $table) {
            $table->dropIndex('relation_parent_id_index');
            $table->dropIndex('relation_parent_type_index');
            $table->dropColumn('parent_type');
            $table->renameColumn('parent_id', 'program_id');
            $table->foreign('program_id', 'relation_ship_paths_program_id_foreign')->references('id')->on('programs');
        });
    }
}
