<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 190)->index();
            $table->double('price',null ,2)->default(1);
            $table->boolean('available')->default(true);

            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')
                ->on('types')->onDelete('cascade');

            $table->unsignedBigInteger('program_id');
            $table->foreign('program_id')->references('id')
                ->on('programs')->onDelete('cascade');

            $table->unsignedBigInteger('media_id');
            $table->foreign('media_id')->references('id')
                ->on('media')->onDelete('cascade');

            $table->unsignedBigInteger('media_type_id');
            $table->foreign('media_type_id')->references('id')
                ->on('media_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slots');
    }
}
