<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Program;
use Faker\Generator as Faker;

$factory->define(Program::class, function (Faker $faker) {
    $channel = \App\Channel::latest('id')->first();

    return [
        'name' => $faker->company,
        'short_name' => $faker->slug(4),
        'description' => $faker->text(100),
        'address' => $faker->address,
        'reach_min' => $min_reach = random_int(300, 1000),
        'reach_max' => random_int($min_reach, 2000),
        'channel_id' => $channel->id,
        'sub_channel_id' => function () use ($faker, $channel) {
            return $channel->subChannel()->first()->id;
        },
        'type_id' => function () use ($faker) {
            return \App\Type::create(['name' => $faker->text(10)])->id;
        },
        'media_id' => function () use ($faker) {
            return factory(\App\Media::class)->create()->id;
        },
        'schedule_id' => function () use ($faker) {
            return \App\Schedule::create(['name' => $faker->text(10)])->id;
        },
        'social_class_id' => function () use ($faker) {
            return \App\SocialClass::create(['name' => $faker->text(10)])->id;
        },
        'literacy_level_id' => function () use ($faker) {
            return \App\LiteracyLevel::create(['name' => $faker->text(10)])->id;
        },
        'interest_id' => function () use ($faker) {
            return \App\Interest::create(['name' => $faker->text(10)])->id;
        },
        'age_group_id' => function () use ($faker) {
            return \App\AgeGroup::create(['name' => $faker->text(10)])->id;
        },
        'profession_level_id' => function () use ($faker) {
            return \App\ProfessionLevel::create(['name' => $faker->text(10)])->id;
        },
        'cost_factor_id' => function () use ($faker) {
            return \App\CostFactor::create(['name' => $faker->text(10)])->id;
        }
    ];
});
