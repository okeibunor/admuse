<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Media;
use Faker\Generator as Faker;

$factory->define(Media::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'short_name' => $faker->slug(3),
        'address' => $faker->address,
        'verified' => $faker->randomElement([true, false]),
        'mobile' => $faker->bankAccountNumber,
        'apcon_number' => $faker->bankAccountNumber,
        'vision' => $faker->text(50),
        'mission' => $faker->text(60)
    ];
});


