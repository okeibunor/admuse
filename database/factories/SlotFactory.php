<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Slot::class, function (Faker $faker) {
    $media_id = 0;

    return [
        'name' => $faker->text(20),
        'price' => $faker->numberBetween(90000),
        'available' => true,
        'type_id' => function () {
            return \App\Type::latest('id')->first()->id;
        },
        'media_id' => $media_id = factory(\App\Media::class)->create()->id,
        'program_id' => factory(\App\Program::class)->create(['media_id' => $media_id])->id,
        'media_type_id' => function () {
            return \App\MediaType::latest('id')->first()->id;
        }
    ];
});

