<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Route::prefix('auth')->group(function (Router $router) {
    $router->post('/new', 'Auth\RegisterController@register');
    $router->post('/open', 'Auth\LoginController@login');
});

Route::prefix('media')->group(function (Router $router) {
    $router->post('create', 'MediaController@create')->middleware(['private', 'publisher']);
    $router->post('create/bulk/csv', 'MediaController@importMediaHouse')->middleware(['private', 'admin']);
    $router->get('list', 'MediaController@list');
    $router->post('type/create', 'TypeController@create')->middleware(['private', 'admin']);
    $router->get('type/list', 'TypeController@list')->middleware('private');
    $router->delete('type/{type_name}', 'TypeController@delete')->middleware(['private', 'admin']);
    $router->patch('type/{type_name}', 'TypeController@edit')->middleware(['private', 'admin']);

    $router->get('/owned', 'MediaController@owned')->middleware(['private', 'publisher']);

    $router->group(['prefix' => '{media_house}', 'middleware' => 'mh'], function (Router $router) {
        $router->patch('', 'MediaController@edit')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->get('contacts', 'ContactController@list')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->post('contacts/create', 'ContactController@create')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->patch('contacts/{contact_name}', 'ContactController@edit')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->delete('contacts/{contact_name}', 'ContactController@delete')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->post('/upload/avatar', 'MediaController@uploadAvatar')
            ->middleware(['private', 'publisher', 'privateMedia']);
        $router->get('/information', 'MediaController@information')->middleware('private');
        $router->get('/slots', 'MediaController@fetchSlotList')->middleware('private');
        $router->group(['prefix' => 'program'], function (Router $router) {
            $router->post('/create', 'ProgramController@create')->middleware(['private', 'publisher']);
            $router->post('/create/bulk/csv', 'ProgramController@bulk')->middleware(['private', 'publisher']);
            $router->get('/list', 'ProgramController@list');
            $router->get('/list/draft', 'ProgramController@drafts');
            $router->group(['prefix' => '{program}', 'middleware' => 'program'], function (Router $router) {
                $router->get('', 'ProgramController@view');
                $router->group(['prefix' => 'slot'], function (Router $router) {
                    $router->post('create', 'SlotController@create')->middleware(['private', 'publisher']);
                    $router->post('create/csv', 'SlotController@bulk')->middleware(['private', 'publisher']);
                    $router->get('list', 'SlotController@list');
                    $router->get('list/draft', 'SlotController@draft');
                });
            });
        });

        $router->post('approval/{item_id}/{status}', 'BookingController@bookingItemStatus')
            ->middleware(['private', 'validBookingItem', 'privateMedia', 'bookingSlotForMedia']);
    });
});

Route::middleware('private')->prefix('user')->group(function (Router $router) {
    $router->get('/profile', 'UserController@profile');
    $router->post('/create', 'AdministratorAction@createNewUser')->middleware('admin');
    $router->group(['prefix' => '{user_id}', 'middleware' => 'validUser'], function (Router $router) {
        $router->post('edit', 'UserController@edit')->middleware(['privateUser', 'admin']);
        $router->get('view', 'UserController@view')->middleware(['admin']);
    });

    $router->get('bank', 'BankDetailController@info');
    $router->get('bank/user_info', 'BankDetailController@getUserBankName');
    $router->post('bank/create', 'BankDetailController@create');
    $router->patch('bank/edit', 'BankDetailController@edit');

    $router->group(['prefix' => 'campaign'], function (Router $router) {
        $router->get('', 'CampaignController@index');
        $router->post('/create', 'CampaignController@store');
        $router->get('/{campaign}', 'CampaignController@show')
            ->middleware(['validCampaign']);
        $router->delete('/{campaign}', 'CampaignController@destroy')
            ->middleware(['validCampaign', 'campaignOwner']);
        $router->post('/{campaign}/payment/wallet', 'CampaignController@payWithWallet')
            ->middleware(['validCampaign', 'campaignOwner']);
        $router->post('/{campaign}/payment/card', 'CampaignController@payWithCard')
            ->middleware(['validCampaign', 'campaignOwner']);
        $router->post('/{campaign}/{slot_id}', 'CampaignItemController@create')
            ->middleware(['validCampaign', 'campaignOwner', 'validSlot']);
        $router->post('/{campaign}/{campaign_item_id}', 'CampaignItemController@update')
            ->middleware(['validCampaign', 'campaignOwner', 'validCampaignItem']);
        $router->delete('/{campaign}/{campaign_item_id}', 'CampaignItemController@remove')
            ->middleware(['validCampaign', 'campaignOwner', 'validCampaignItem']);
    });

    $router->get('transactions', 'WalletController@getTransactionHistory');
    $router->post('wallet/credit', 'WalletController@fund');
    $router->post('wallet/verify/{reference}', 'WalletController@claim');
    $router->post('wallet/withdraw', 'WithdrawalRequestController@withdraw');
    $router->get('wallet/withdraws', 'WithdrawalRequestController@list');
    $router->patch('wallet/withdraws/respond', 'WithdrawalRequestController@respond')
        ->middleware('admin');
});

Route::prefix('cart')->group(function (Router $router) {
    $router->post('book', 'BookingController@create')->middleware('private');
});

Route::prefix('booking')->group(function (Router $router) {
    $router->get('list', 'BookingController@list')->middleware('private');
    $router->get('{booking_id}', 'BookingController@single')->middleware(['private', 'validBooking', 'bookingOwner']);
    $router->get('{media_house}/list', 'BookingController@getMediaHouseBookings')
        ->middleware(['private', 'mh', 'publisher', 'privateMedia']);
    $router->post('{booking_id}/{item_id}/edit', 'BookingController@edit')
        ->middleware(['private', 'validBooking', 'bookingOwner', 'validBookingItem']);
    $router->get('{booking_id}/{item_id}', 'BookingController@getBookingInformation')
        ->middleware(['private', 'validBooking', 'bookingOwner', 'validBookingItem']);

    $router->get('calendar', 'CalendarController@getEventsForThisMonth')
        ->middleware('private');

    $router->group(['prefix' => '{booking_id}/pay', 'middleware' => ['private', 'validBooking']], function ($router) {
        $router->post('wallet', 'BookingController@payFromWallet');
        $router->post('card', 'BookingController@payFromCard');
    });
});

Route::prefix('file')->group(function (Router $router) {
    $router->delete('{file_id}/delete', 'FileController@delete')
        ->middleware(['private', 'validFile', 'fileOwner']);
    $router->post('/upload/images', 'FileController@uploadImages')->middleware(['private', 'publisher']);
});

Route::get('channel/list', 'ChannelController@list');
Route::prefix('channel')->middleware(['private', 'admin'])->group(function (Router $router) {
    $router->post('create', 'ChannelController@create');
    $router->patch('{channel}', 'ChannelController@edit');
    $router->delete('{channel}', 'ChannelController@delete');
});
Route::get('channel/{channel}/subchannels', 'SubChannelController@list');
Route::prefix('channel/{channel}/subchannels')->middleware(['private', 'admin'])->group(function (Router $router) {
    $router->post('create', 'SubChannelController@create');
    $router->patch('{subchannel}', 'SubChannelController@edit');
    $router->delete('{subchannel}', 'SubChannelController@delete');
});
Route::get('channel/{channel}/media/list', 'ChannelController@getMediaList');
Route::get('schedule/list', 'ScheduleController@list');
Route::post('schedule/create', 'ScheduleController@create')->middleware(['private', 'admin']);
Route::patch('schedule/{schedule}', 'ScheduleController@edit')->middleware(['private', 'admin']);
Route::delete('schedule/{schedule}', 'ScheduleController@delete')->middleware(['private', 'admin']);
Route::get('social_class/list', 'SocialClassController@list');
Route::group(['prefix' => 'social_class', 'middleware' => ['private', 'admin']], function (Router $router) {
    $router->post('create', 'SocialClassController@create');
    $router->patch('{socialClass}', 'SocialClassController@edit');
    $router->delete('{socialClass}', 'SocialClassController@delete');
});
Route::get('literacy_level/list', 'LiteracyLevelController@list');
Route::post('literacy_level/create', 'LiteracyLevelController@create')->middleware(['private', 'admin']);
Route::patch('literacy_level/{literacyLevel}', 'LiteracyLevelController@edit')->middleware(['private', 'admin']);
Route::delete('literacy_level/{literacyLevel}', 'LiteracyLevelController@delete')->middleware(['private', 'admin']);
Route::get('interest/list', 'InterestController@list');
Route::post('interest/create', 'InterestController@create')->middleware(['private', 'admin']);
Route::patch('interest/{interest}', 'InterestController@edit')->middleware(['private', 'admin']);
Route::delete('interest/{interest}', 'InterestController@delete')->middleware(['private', 'admin']);
Route::get('age_group/list', 'AgeGroupController@list');
Route::post('age_group/create', 'AgeGroupController@create')->middleware(['private', 'admin']);
Route::patch('age_group/{ageGroup}', 'AgeGroupController@edit')->middleware(['private', 'admin']);
Route::delete('age_group/{ageGroup}', 'AgeGroupController@delete')->middleware(['private', 'admin']);
Route::get('professional_level/list', 'ProfessionalLevelController@list');
Route::post('professional_level/create', 'ProfessionalLevelController@create')->middleware(['private', 'admin']);
Route::patch('professional_level/{professionLevel}', 'ProfessionalLevelController@edit')->middleware(['private', 'admin']);
Route::delete('professional_level/{professionLevel}', 'ProfessionalLevelController@delete')->middleware(['private', 'admin']);
Route::get('size/list', 'SizeController@getAll');
Route::post('size/create', 'SizeController@create')->middleware(['private', 'admin']);
Route::patch('size/{size}', 'SizeController@edit')->middleware(['private', 'admin']);
Route::delete('size/{size}', 'SizeController@delete')->middleware(['private', 'admin']);
Route::get('cost_factor/list', 'CostFactorController@list');
Route::post('cost_factor/create', 'CostFactorController@create')->middleware(['private', 'admin']);
Route::patch('cost_factor/{costFactor}', 'CostFactorController@edit')->middleware(['private', 'admin']);
Route::delete('cost_factor/{costFactor}', 'CostFactorController@delete')->middleware(['private', 'admin']);
Route::get('gender/list', 'GenderController@list');
Route::post('gender/create', 'GenderController@create')->middleware(['private', 'admin']);
Route::delete('gender/{type_name}', 'GenderController@delete')->middleware(['private', 'admin']);
Route::patch('gender/{type_name}', 'GenderController@edit')->middleware(['private', 'admin']);
Route::get('marital_status/list', 'MaritalController@list');
Route::post('marital_status/create', 'MaritalController@create')->middleware(['private', 'admin']);
Route::delete('marital_status/{type_name}', 'MaritalController@delete')->middleware(['private', 'admin']);
Route::patch('marital_status/{type_name}', 'MaritalController@edit')->middleware(['private', 'admin']);
Route::get('types/list', 'TypeController@list');
Route::post('types/create', 'TypeController@create')->middleware(['private', 'admin']);
Route::delete('types/{type_name}', 'TypeController@delete')->middleware(['private', 'admin']);
Route::patch('types/{type_name}', 'TypeController@edit')->middleware(['private', 'admin']);

Route::get('slots', 'SlotController@fetch');
Route::get('/slot/list', 'SlotController@all')->middleware(['private', 'admin']);
Route::prefix('slot/{slot_id}')->middleware('validSlot')->group(function (Router $router) {
    $router->get('', 'SlotController@getByID');
    $router->patch('', 'SlotController@edit')->middleware(['private', 'publisher', 'slotOwner']);
    $router->get('reviews', 'ReviewController@index');
    $router->post('review', 'ReviewController@create')->middleware('private');
    $router->patch('review', 'ReviewController@edit')->middleware('private');
    $router->delete('review', 'ReviewController@delete')->middleware('private');
});
Route::get('slots/search', 'SlotController@search');
Route::get('/program/list', 'ProgramController@all')->middleware(['private', 'admin']);
Route::get('users/all', 'UserController@all')->middleware(['private', 'admin']);
Route::get('callback/paystack/booking', 'BookingController@verifyBookingPaymentByCard')->name('verifyBookingPaymentByCard');
Route::get('callback/paystack/campaign', 'CampaignController@verifyCardPayment')->name('verifyCampaignCardPayment');
Route::get('overall', 'InsightController@dashboard')->middleware(['private', 'admin']);
Route::get('media_types', 'MediaTypeController@all');
