<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\MessageBag;

class ValidationException extends Exception
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * ValidationException constructor.
     * @param $validationInstance MessageBag|Validator
     */
    public function __construct($validationInstance)
    {
        parent::__construct("Validation Error!");


        if ($validationInstance instanceof Validator)
            $this->errors = $validationInstance->errors();
        else
            $this->errors = $validationInstance;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
