<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 * @property mixed bank_name
 * @property mixed bank_code
 * @property mixed number
 * @property mixed user_id
 * @property User user
 * @property mixed recipient_code
 */
class BankDetail extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
