<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * @method static self|Builder firstOrCreate(array $array)
 * @method static self|Builder where(string $string, $channel)
 * @method static self create(array $array)
 * @property mixed name
 * @property SubChannel subChannel
 */
class Channel extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public static function name(string $name)
    {
        if ($exist = self::where('name', $name)->first())
            return $exist;

        throw new ModelNotFoundException("Invalid Channel");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subChannel()
    {
        return $this->hasMany(SubChannel::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
