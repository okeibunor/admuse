<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @method static self|Builder create(array $array)
 * @method static self|Builder where(string $string, string $short_name)
 * @method static self|Builder join(string ...$string)
 * @method static self|LengthAwarePaginator paginate(int $int)
 * @property mixed name
 * @property mixed description
 * @property mixed short_name
 * @property mixed address
 * @property mixed user_id
 * @property User user
 * @property mixed id
 * @property mixed verified
 * @property mixed mobile
 * @property mixed email
 * @property mixed contact_person
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property mixed apcon_number
 * @property mixed avatar
 */
class Media extends Model
{
    protected $guarded = [];

    public static function getHouse(string $short_name)
    {
        return self::where('short_name', $short_name)->first();
    }

    /**
     * @param int $fetchCount
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getMyHouses(int $fetchCount)
    {
        return self::where('user_id', @$GLOBALS['user']->id)->paginate($fetchCount);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function slots()
    {
        return $this->hasMany(Slot::class);
    }

    public function program()
    {
        return $this->hasMany(Program::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }
}
