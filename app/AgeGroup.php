<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgeGroup extends Model
{

    public $timestamps = false;
    protected $guarded = [];

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
