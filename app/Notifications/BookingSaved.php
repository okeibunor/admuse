<?php

namespace App\Notifications;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingSaved extends Notification
{
    use Queueable;
    /**
     * @var Booking
     */
    private $booking;

    /**
     * Create a new notification instance.
     *
     * @param Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You made a booking')
            ->greeting('Dear ' . $this->booking->user->name)
            ->line('Your booking with booking ID \'' . $this->booking->id .'\' has been received.
Kindly proceed to the next step in the process to ensure your needs are met expeditiously.')
            ->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs.');
    }
}
