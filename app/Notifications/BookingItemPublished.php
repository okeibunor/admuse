<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\BookingSlot;

class BookingItemPublished extends Notification
{
    use Queueable;

    /**
     * @var BookingSlot
     */
    private $slot;

    public function __construct(BookingSlot $slot)
    {
        $this->slot = $slot;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your ad has been published')
            ->greeting('Dear '. $this->slot->booking->user->name . ',')
            ->line('We are glad to inform you that your ad for ' . $this->slot->slot->media->name . ' has been published.')
            ->line(preg_quote($this->slot->media_house_note))
            ->line('You can provide feedback on this booking from your account.')
            ->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs.');
    }
}
