<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Welcome extends Notification
{
    use Queueable;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new notification instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Welcome to Hypedude!')
            ->greeting('Dear ' . $this->user->name)
            ->line('I am very happy to welcome you to the MEA’s largest and most trusted media market place.')
            ->line('To help you get started, the team and I have put together this short clip [hyperlink to video on website or youtube].')
            ->line('You may contact us directly if you have any urgent requests.')
            ->line('I look forward to serving your marketing and advertising needs, as does the team.')
            ->line('Again, welcome!')
            ->salutation(nl2br('Sincerely,' . PHP_EOL . 'Chuks E. Okeibunor' . PHP_EOL . 'Chief Dude @ Hypedude'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
