<?php

namespace App\Notifications;

use App\Booking;
use App\BookingSlot;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingItemRejected extends Notification
{
    use Queueable;
    /**
     * @var BookingSlot
     */
    private $slot;

    public function __construct(BookingSlot $slot)
    {
        $this->slot = $slot;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your ad request has been declined')
            ->greeting('Dear '. $this->slot->booking->user->name . ',')
            ->line('We regret to inform you that your ad for [Publisher] has been reviewed and declined for the following reason:')
            ->line('"' . $this->slot->media_house_note . '"')
            ->line('Typically, in these kinds of situations, we find another publisher who targets the same audience demographics.')
            ->line('Our support team will contact you shortly to discuss other options and next steps.')
            ->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs');
    }
}
