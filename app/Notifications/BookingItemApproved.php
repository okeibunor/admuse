<?php

namespace App\Notifications;

use App\Booking;
use App\BookingSlot;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingItemApproved extends Notification
{
    use Queueable;
    /**
     * @var BookingSlot
     */
    private $slot;

    public function __construct(BookingSlot $slot)
    {
        $this->slot = $slot;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your ad content has been approved')
            ->greeting('Dear '. $this->slot->booking->user->name . ',')
            ->line('Your ad material for ' . $this->slot->slot->media->name . ' have been reviewed and approved.')
            ->line('You will be notified of the publication date.')
            ->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs');
    }
}
