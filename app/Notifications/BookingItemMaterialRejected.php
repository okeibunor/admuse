<?php

namespace App\Notifications;

use App\BookingSlot;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingItemMaterialRejected extends Notification
{
    use Queueable;
    /**
     * @var BookingSlot
     */
    private $slot;

    /**
     * Create a new notification instance.
     *
     * @param BookingSlot $slot
     */
    public function __construct(BookingSlot $slot)
    {
        //
        $this->slot = $slot;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Your ad content has some issues')
            ->greeting('Dear '. $this->slot->booking->user->name . ',')
            ->line('Your ad material for ' . $this->slot->slot->media->name . ' have been reviewed and found with the following issues')
            ->line(preg_quote($this->slot->media_house_note))
            ->line('Please log into your account to supply another material or offer a response.')
            ->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs.');
    }
}
