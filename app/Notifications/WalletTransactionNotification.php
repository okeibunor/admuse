<?php

namespace App\Notifications;

use App\WalletTransaction;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WalletTransactionNotification extends Notification
{
    use Queueable;
    /**
     * @var WalletTransaction
     */
    private $transaction;

    /**
     * Create a new notification instance.
     *
     * @param WalletTransaction $transaction
     */
    public function __construct(WalletTransaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->greeting('Dear ' . $this->transaction->user->name . ',');

        if ($this->transaction->type == 'debit') {
            $message->subject('You spent ' . $this->transaction->amount . ' from your wallet')
                ->line('You just spent ' . $this->transaction->amount . ' from your wallet. You may log into your account to view the transaction and to top up your wallet.');
        } else {
            $message->subject('You topped up your wallet')
                ->line('You have successfully topped up your wallet.');
        }

        $message->line('If you require any assistance, please contact us support@hypedude.com ')
            ->line('We look forward to serving your advertising needs');

        return $message;
    }
}
