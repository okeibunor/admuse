<?php

namespace App\Notifications;

use App\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WithdrawApproved extends Notification
{
    use Queueable;
    /**
     * @var WithdrawalRequest
     */
    private $withdrawalRequest;

    /**
     * Create a new notification instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        //
        $this->withdrawalRequest = $withdrawalRequest;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Withdrawal Approved')
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
