<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Contact
 * @package App
 * @property mixed id
 * @property mixed media_id
 * @property mixed name
 * @property mixed mobile
 * @property Media media
 * @method static Builder where(string $string, string $route)
 */
class Contact extends Model
{
    public $timestamps = false;
    protected $table = 'contact';
    protected $guarded = [];

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mobile' => $this->mobile
        ];
    }
}
