<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @method static self|Builder create(array $array)
 * @method static self|LengthAwarePaginator paginate(int $int)
 * @method static Builder where(string $string,string $route)
 */
class Type extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
