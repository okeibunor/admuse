<?php

namespace App\Http\Middleware;

use Closure;

class CampaignOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $campaign = getRequestedCampaign();

        if ($campaign->user_id === getUser()->id) {
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Access Denied',
            'message' => 'Campaign belongs to another user.'
        ], 401);
    }
}
