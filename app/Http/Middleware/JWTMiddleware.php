<?php

namespace App\Http\Middleware;

use Closure;

class JWTMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        switch (tokenStatus()) {
            case "user_not_found":
            case "token_expired":
            case "token_invalid":
                return sendResponse("error", ["message" => "Expired Session"], 401);
            case "token_absent":
                return sendResponse("error", ["message" => "Unauthorized"], 401);

        }

        return $next($request);
    }
}
