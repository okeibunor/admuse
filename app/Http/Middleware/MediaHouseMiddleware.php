<?php

namespace App\Http\Middleware;

use App\Media;
use Closure;

class MediaHouseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $mediaHouse = $request->route('media_house');

        if (!$mediaHouse) {
            return sendResponse('error', "Middleware Not Allowed On Route");
        } else {
            if ($house = Media::getHouse($mediaHouse)) {
                global $mediaHouse;
                $mediaHouse = $house;
            } else {
                return sendResponse('error', "Media House Not Found");
            }
        }

        return $next($request);
    }
}
