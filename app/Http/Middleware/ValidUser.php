<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class ValidUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id = $request->route('user_id');
        $user = User::where('id', $user_id)->first();
        if ($user) {
            $GLOBALS['requestedUser'] = $user;
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Not Found',
            'message' => 'User Not Found'
        ], 404);
    }
}
