<?php

namespace App\Http\Middleware;

use App\Program;
use Closure;

class ProgramMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $program = $request->route('program');

        if (!$program) return sendResponse('error', "Middleware Not Allowed On Route");
        else {
            if ($theProgram = Program::getProgram($program)) {
                global $activeProgram;
                $activeProgram = $theProgram;
            } else {
                return sendResponse('error', "Program Not Found");
            }
        }

        return $next($request);
    }
}
