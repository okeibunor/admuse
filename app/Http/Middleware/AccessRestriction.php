<?php

namespace App\Http\Middleware;

use Closure;

class AccessRestriction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $restriction
     * @param null $adminPermission
     * @return mixed
     */
    public function handle($request, Closure $next, $restriction = "publisher", $adminPermission = null)
    {

        $userAccountType = getUser()->type->name;

        /**
         * Converting the allowed account type from its form to an array.
         * This is to help with checking by using in_array function.
         *
         * @var $restrictions array
         */
        $restrictions = explode("|", $restriction);

        /**
         * Check if the user is having one of the allowed account type as requested
         */
        if (in_array($userAccountType, $restrictions)) {

            /**
             * Let check if we want any specific permission to be granted to admin
             * before they can access the route also
             *
             * Note: We are not checking for publisher or affiliate or any other account type
             * since they are not using permission.
             */
            if ($adminPermission) {

                //TODO: Work on this

                return sendResponse('error', [
                    'type' => 'TODO: Work on this',
                    'message' => 'TODO: Work on this'
                ]);
            }

            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Access Denied',
            'message' => ucwords($userAccountType) . " Doesn't Have Access To Route"
        ], 401);

    }
}
