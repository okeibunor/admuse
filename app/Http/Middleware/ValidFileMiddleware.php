<?php

namespace App\Http\Middleware;

use App\File;
use Closure;

class ValidFileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $fileId = $request->route('file_id');

        if ($file = File::where('id', $fileId)->first()) {
            $GLOBALS['requestedFile'] = $file;
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Not Found',
            'message' => 'File Not Found'
        ], 404);

    }
}
