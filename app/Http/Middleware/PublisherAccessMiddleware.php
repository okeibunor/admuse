<?php

namespace App\Http\Middleware;

use App\AccountType;
use Closure;

class PublisherAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /**
         * Giving access to admin
         */
        if (getUser()->type->id === AccountType::admin()->id) {
            return $next($request);
        }

        if (getUser()->type->id !== AccountType::publisher()->id) {
            return sendResponse('error', [
                'type' => 'Access Denied',
                'message' => "User Not A Publisher"
            ], 401);
        }
        return $next($request);
    }
}
