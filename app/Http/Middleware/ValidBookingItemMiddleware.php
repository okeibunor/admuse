<?php

namespace App\Http\Middleware;

use App\BookingSlot;
use Closure;

class ValidBookingItemMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bookingItem = BookingSlot::where('id', $request->route('item_id'))
            ->first();

        if ($bookingItem) {
            $GLOBALS['currentBookingItem'] = $bookingItem;
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Not Found',
            'message' => 'Booking Item Not Found'
        ], 404);
    }
}
