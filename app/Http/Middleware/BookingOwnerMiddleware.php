<?php

namespace App\Http\Middleware;

use App\AccountType;
use Closure;

class BookingOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $booking = getCurrentBooking();
        $user = getUser();

        if ($user->id === $booking->user_id
            || $user->id === AccountType::admin()->id) return $next($request);

        return sendResponse('error', [
            'type' => 'Access Denied',
            'message' => 'Booking Not Owned By User'
        ], 401);
    }
}
