<?php

namespace App\Http\Middleware;

use App\Exceptions\Custom;
use App\Slot;
use Closure;
use Illuminate\Http\Response;

class ValidSlotMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws Custom
     */
    public function handle($request, Closure $next)
    {
        $slot = Slot::where('id', $request->route('slot_id'))
            ->first();

        if (!$slot) return sendResponse('error',
            [
                'type' => 'Not Found',
                'message' => 'Slot Not Found'
            ], Response::HTTP_NOT_FOUND);

        $GLOBALS['requestedSlot'] = $slot;

        return $next($request);
    }
}
