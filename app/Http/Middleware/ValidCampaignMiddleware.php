<?php

namespace App\Http\Middleware;

use App\Campaign;
use Closure;

class ValidCampaignMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $campaign_id = $request->route('campaign');
        $campaign = Campaign::where('id', $campaign_id)->first();

        if ($campaign) {
            $GLOBALS['requestedCampaign'] = $campaign;
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Not Found',
            'message' => 'Campaign Not Found',
        ], 404);
    }
}
