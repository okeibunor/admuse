<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class AffiliateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = getUser();
        if($user->isAffiliate()){
            return $next($request);
        }

        return \sendResponse('error',[
            'type' => 'Access Denied',
            'message' => 'Affiliate Only Route'
        ], 401);
    }
}
