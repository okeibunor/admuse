<?php

namespace App\Http\Middleware;

use App\AccountType;
use Closure;

class MediaHouseOwnersAccessOnlyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = getUser();
        $userTypeId = $user->type->id;

        if ($userTypeId === AccountType::admin()->id) {
            return $next($request);
        }

        if ($user->id !== getMediaHouse()->user_id) {
            return sendResponse('error', [
                'type' => 'Access Denied',
                'message' => 'Media House Not Owned By You'
            ], 401);
        }

        return $next($request);
    }
}
