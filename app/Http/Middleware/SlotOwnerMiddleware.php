<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SlotOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (getRequestedSlot()->media_id === getUser()->media->id
            || getUser()->isAdmin()) {
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Access Denied',
            'message' => 'Slot belongs to another publication'
        ], 401);
    }
}
