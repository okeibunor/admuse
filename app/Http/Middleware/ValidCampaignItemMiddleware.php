<?php

namespace App\Http\Middleware;

use App\CampaignItem;
use Closure;

class ValidCampaignItemMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->get('campaign_item_id');

        /**
         * @var CampaignItem $campaignItem
         */
        $campaignItem = getRequestedCampaign()->items()
            ->where('id', $id)->first();

        if ($campaignItem) {
            $GLOBALS['requestedCampaignItem'] = $campaignItem;
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Not Found',
            'message' => 'Campaign Item Not Found'
        ], 404);
    }
}
