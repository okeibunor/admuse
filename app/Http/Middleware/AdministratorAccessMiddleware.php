<?php

namespace App\Http\Middleware;

use App\AccountType;
use Closure;

class AdministratorAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (getUser()->type->id !== AccountType::admin()->id) {
            return sendResponse('error', [
                'type' => 'Access Denied',
                'message' => "User Not An Administrator'"
            ], 401);
        }

        return $next($request);
    }
}
