<?php

namespace App\Http\Middleware;

use App\AccountType;
use Closure;

class FileOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $file = getRequestedFile();
        $user = getUser();

        if ($user->id !== $file->user_id)
            return sendResponse('error', [
                'type' => 'Access Denied',
                'message' => 'Only Owner Have Access To File'
            ], 401);

        return $next($request);
    }
}
