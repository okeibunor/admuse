<?php

namespace App\Http\Middleware;

use App\Booking;
use Closure;

class ValidBookingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bookingId = $request->route('booking_id');
        $booking = Booking::where('id', $bookingId)->first();

        //set current booking
        $GLOBALS['currentBooking'] = $booking;

        if ($booking) return $next($request);

        return sendResponse('error',[
            'type' => 'Not Found',
            'message' => 'Invalid Booking Link'
        ], 401);
    }
}
