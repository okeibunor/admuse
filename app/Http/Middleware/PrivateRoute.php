<?php

namespace App\Http\Middleware;

use Closure;

class PrivateRoute
{
    /**
     * Handle an incoming request.
     *
     * Make sure that the user sending this request is the same user
     * who is being requested in the route `user_id`
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = getRequestedUser();
        $me = getUser();

        if ($me->isAdmin()) return $next($request);

        if ($user->id === $me->id) {
            return $next($request);
        }

        return sendResponse('error', [
            'type' => 'Access Denied',
            'message' => 'Can\'t Access Another User'
        ], 401);
    }
}
