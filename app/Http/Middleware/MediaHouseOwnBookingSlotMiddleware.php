<?php

namespace App\Http\Middleware;

use Closure;

class MediaHouseOwnBookingSlotMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bookingSlot = getRequestedBookingItem();
        $mediaHouse = getMediaHouse();

        if ($bookingSlot->slot->media_id !== $mediaHouse->id)
            return sendResponse('error', [
                'type' => 'Access Denied',
                'message' => 'Slot Belongs To ' . $mediaHouse->name
            ], 401);

        return $next($request);
    }
}
