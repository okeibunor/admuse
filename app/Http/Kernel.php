<?php

namespace App\Http;

use App\Http\Middleware\AccessRestriction;
use App\Http\Middleware\AdministratorAccessMiddleware;
use App\Http\Middleware\Authenticate;
use App\Http\Middleware\BookingOwnerMiddleware;
use App\Http\Middleware\CampaignOwnerMiddleware;
use App\Http\Middleware\CrossOriginMiddleware;
use App\Http\Middleware\EncryptCookies;
use App\Http\Middleware\FileOwnerMiddleware;
use App\Http\Middleware\JWTMiddleware;
use App\Http\Middleware\MediaHouseMiddleware;
use App\Http\Middleware\MediaHouseOwnBookingSlotMiddleware;
use App\Http\Middleware\MediaHouseOwnersAccessOnlyMiddleware;
use App\Http\Middleware\PrivateRoute;
use App\Http\Middleware\ProgramMiddleware;
use App\Http\Middleware\PublisherAccessMiddleware;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Http\Middleware\SlotOwnerMiddleware;
use App\Http\Middleware\ValidBookingItemMiddleware;
use App\Http\Middleware\ValidBookingMiddleware;
use App\Http\Middleware\ValidCampaignItemMiddleware;
use App\Http\Middleware\ValidCampaignMiddleware;
use App\Http\Middleware\ValidFileMiddleware;
use App\Http\Middleware\ValidSlotMiddleware;
use App\Http\Middleware\ValidUser;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Auth\Middleware\AuthenticateWithBasicAuth;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Auth\Middleware\EnsureEmailIsVerified;
use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Illuminate\Routing\Middleware\ValidateSignature;
use Illuminate\Session\Middleware\AuthenticateSession;
use Illuminate\Session\Middleware\StartSession;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class Kernel extends HttpKernel
{
    /**
     * 'private' => JWTMiddleware::class
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            EncryptCookies::class,
            AddQueuedCookiesToResponse::class,
            StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            ShareErrorsFromSession::class,
            VerifyCsrfToken::class,
            SubstituteBindings::class,
        ],

        'api' => [
            'throttle:160,1',
            'bindings',
            'cor'
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => Authenticate::class,
        'auth.basic' => AuthenticateWithBasicAuth::class,
        'bindings' => SubstituteBindings::class,
        'cache.headers' => SetCacheHeaders::class,
        'can' => Authorize::class,
        'guest' => RedirectIfAuthenticated::class,
        'signed' => ValidateSignature::class,
        'throttle' => ThrottleRequests::class,
        'verified' => EnsureEmailIsVerified::class,
        'private' => JWTMiddleware::class,
        'cor' => CrossOriginMiddleware::class,
        'mh' => MediaHouseMiddleware::class,
        'media_house' => MediaHouseMiddleware::class,
        'program' => ProgramMiddleware::class,
        'admin' => AdministratorAccessMiddleware::class,
        'publisher' => PublisherAccessMiddleware::class,
        'allowed' => AccessRestriction::class,
        'privateMedia' => MediaHouseOwnersAccessOnlyMiddleware::class,
        'validBooking' => ValidBookingMiddleware::class,
        'bookingOwner' => BookingOwnerMiddleware::class,
        'validBookingItem' => ValidBookingItemMiddleware::class,
        'validFile' => ValidFileMiddleware::class,
        'fileOwner' => FileOwnerMiddleware::class,
        'bookingSlotForMedia' => MediaHouseOwnBookingSlotMiddleware::class,
        'validUser' => ValidUser::class,
        'privateUser' => PrivateRoute::class,
        'slotOwner' => SlotOwnerMiddleware::class,
        'validSlot' => ValidSlotMiddleware::class,
        'validCampaign' => ValidCampaignMiddleware::class,
        'campaignOwner' => CampaignOwnerMiddleware::class,
        'validCampaignItem' => ValidCampaignItemMiddleware::class
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        StartSession::class,
        ShareErrorsFromSession::class,
        Authenticate::class,
        AuthenticateSession::class,
        SubstituteBindings::class,
        Authorize::class,
    ];
}
