<?php

namespace App\Http\Library;


class BookingStatus
{
    public static $UNDECIDED = 'undecided';
    public static $COMPLETED = 'completed';
    public static $PROCESSING = 'processing';
}
