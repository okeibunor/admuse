<?php

namespace App\Http\Library;


use App\Exceptions\Custom;
use App\User;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Ramsey\Uuid\Uuid;

class PayStack
{
    private $transactionInitURL = 'https://api.paystack.co/transaction/initialize';
    private $transactionInfoURL = 'https://api.paystack.co/transaction/verify/';
    private $transferRecipient = 'https://api.paystack.co/transferrecipient';
    private $transfer = 'https://api.paystack.co/transfer';
    private $accountNameQuery = 'https://api.paystack.co/bank/resolve';

    /**
     * @param $amount
     * @param User $user
     * @param null|array $otherMetaData
     * @param null|string $callback_url
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public static function initTransaction($amount, User $user, $otherMetaData = null, $callback_url = null)
    {
        $instance = new self();

        return $instance->post(
            $instance->transactionInitURL,
            [
                "amount" => self::getAmountWithTransactionFee($amount) * 100,
                "email" => $user->email,
                'callback_url' => $callback_url,
                'metadata' => $user->stringnifiedForPayment($otherMetaData)
            ]
        );
    }

    public static function getAmountWithTransactionFee(int $amount)
    {
        return $amount + self::getTransactionFee($amount);
    }

    public static function getTransactionFee(int $amount)
    {
        // add transaction fee
        $transactionFee = $amount / 100 * config('payment.transaction_fee.percent');

        if($transactionFee > (int)config('payment.transaction_fee.max')) $transactionFee = (int)config('payment.transaction_fee.max');
        
        return $transactionFee;
    }

    /**
     * @param $link
     * @param $data
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public function post($link, $data)
    {
        $response = $this->request("post", $link, $data);

        if ($response->getStatusCode()) return json_decode($response->getBody());

        throw new Custom('Internal Server Error: ' . $response->getBody());
    }

    /**
     * @param $method
     * @param $link
     * @param $data
     * @return mixed|ResponseInterface
     * @throws GuzzleException
     */
    public function request($method, $link, $data = null)
    {
        $client = new Client;
        return $client->request($method, $link, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . env('PAYSTACK_SECRET_KEY')
            ],
            'body' => json_encode($data)
        ]);
    }

    /**
     * @param $transactionKey
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public static function transactionInfo($transactionKey)
    {
        $instance = new self;
        $response = $instance->get(
            $instance->transactionInfoURL . $transactionKey
        );

        return $response;
    }

    /**
     * @param $link
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public function get($link)
    {
        $response = $this->request("get", $link);

        if ($response->getStatusCode()) return json_decode($response->getBody());

        throw new Custom('Internal Server Error: ' . $response->getBody());
    }

    /**
     * @param float $amount
     * @param User $user
     * @throws Custom
     * @throws GuzzleException
     * @throws Exception
     */
    public static function transfer(float $amount, User $user)
    {
        $instance = new self;
        $instance->post(
            $instance->transfer, [
            'source' => 'balance',
            'amount' => $amount,
            'recipient' => $user->bankDetail->recipient_code,
            'reference' => Uuid::uuid1()->toString()
        ]);
    }

    /**
     * @param $name
     * @param $accountNumber
     * @param $bankCode
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public static function createTransferRecipient($name, $accountNumber, $bankCode)
    {
        $instance = new self;
        $data = [
            "type" => "nuban",
            "name" => $name,
            "description" => "bank account",
            "account_number" => $accountNumber,
            "bank_code" => $bankCode,
            "currency" => "NGN"
        ];

        return $instance->post(
            $instance->transferRecipient,
            $data
        );
    }

    /**
     * @param $accountNumber
     * @param $bankCode
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public static function _getAccountName($accountNumber, $bankCode)
    {
        return (new self)->getAccountName($accountNumber, $bankCode);
    }

    /**
     * @param $accountNumber
     * @param $bankCode
     * @return mixed
     * @throws Custom
     * @throws GuzzleException
     */
    public function getAccountName($accountNumber, $bankCode)
    {
        return $this->get(
            $this->accountNameQuery . '?account_number=' . $accountNumber . '&bank_code=' . $bankCode
        );
    }
}
