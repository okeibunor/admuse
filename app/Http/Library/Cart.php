<?php

namespace App\Http\Library;


use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\Slot;
use Illuminate\Support\MessageBag;

class Cart
{

    /**
     * @throws Custom
     * @throws ValidationException
     */
    public static function get()
    {
        $items = request('items', []);

        if (count($items) < 1) throw new Custom("Empty Cart Item");

        return self::checkItemAvailability($items);
    }

    /**
     * @param array $items
     * @return Slot[]
     * @throws ValidationException
     */
    private static function checkItemAvailability(array $items)
    {
        $valid = [];
        $invalid = [];

        foreach ($items as $item) {
            //check if the item is currently valid
            $slot = Slot::where("id", $item)->first();

            //if the slot is valid, let's save it
            if ($slot) {
                $valid[] = $slot;
                continue;
            }

            $invalid[] = $item;
        }

        self::sendInValidSlotMessageIfAny($invalid);

        return $valid;
    }

    /**
     * @param array $invalid
     * @throws ValidationException
     */
    private static function sendInValidSlotMessageIfAny(array $invalid)
    {
        if (count($invalid) > 0) {
            $bag = new MessageBag();

            foreach ($invalid as $item) {
                $bag->add((string)$item, "Slot With ID $item, Not Found");
            }

            throw new ValidationException($bag);
        }
    }

}
