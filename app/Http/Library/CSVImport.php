<?php

namespace App\Http\Library;


class CSVImport
{

    private static $name = null;
    private static $email = null;
    private static $description = null;
    private static $address = null;
    private static $apcon_number = null;
    private static $mobile = null;
    private static $mission = null;
    private static $vision = null;
    private static $mediaHouseHeaders = ['name', 'email', 'description', 'address', 'apcon_number', 'mobile', 'mission', 'vision'];
    private static $programHeaders = ['name', 'description', 'reach_min', 'reach_max', 'address', 'location'];
    private static $slotHeaders = ['name', 'price', 'media_type'];
    private static $reach_min;
    private static $media_type;
    private static $price;
    private static $reach_max;
    private static $location;

    public static function getEachLine($indexSetCallback, $returnValueCallback)
    {
        $isHeader = true;
        $handler = fopen(request()->file('csv'), 'r');

        while ($line = fgetcsv($handler)) {
            // setting the index of the email address field
            // and the index where name is (if available)
            if ($isHeader) {
                //because we don't know what else the csv file might contain
                // we have to loop through the columns too
                foreach (array_keys($line) as $key) {
                    self::$indexSetCallback($line, $key);
                }
                $isHeader = false;
                continue;
            }

            // we should have skipped the
            // remaining process since the above is only ran once
            yield self::$returnValueCallback($line);
        }
    }

    public static function provideMediaHouseData(array $line)
    {
        return [
            'name' => self::trimValue(@$line[self::$name]),
            'email' => self::trimValue(@$line[self::$email]),
            'description' => self::trimValue(@$line[self::$description]),
            'address' => self::trimValue(@$line[self::$address]),
            'apcon_number' => self::trimValue(@$line[self::$apcon_number]),
            'mobile' => self::trimValue(@$line[self::$mobile]),
            'mission' => self::trimValue(@$line[self::$mission]),
            'vision' => self::trimValue(@$line[self::$vision]),
        ];
    }

    private static function trimValue($value)
    {
        return trim($value, '"\'');
    }

    private static function convertToArray($value)
    {
        $data = collect(explode(",", $value));
        return $data->map(function (string $each) {
            return self::trimValue($each);
        });
    }

    private static function setMediaHouseIndex(array $line, $index)
    {
        self::indexSetter(self::$mediaHouseHeaders, $line, $index);
    }

    private static function indexSetter(array $headers, array $line, $index)
    {
        foreach ($headers as $header) {
            self::$$header = $line[$index] === $header ? $index : self::$$header;
        }
    }

    private static function setProgramIndex($line, $index)
    {
        self::indexSetter(self::$programHeaders, $line, $index);
    }

    private static function setSlotIndex($line, $index)
    {
        self::indexSetter(self::$slotHeaders, $line, $index);
    }

    private static function programData(array $row)
    {
        return [
            'name' => self::trimValue($row[self::$name]),
            'description' => self::trimValue(@$row[self::$description]),
            'address' => self::trimValue(@$row[self::$address]),
            'reach_min' => self::trimValue($row[self::$reach_min]),
            'reach_max' => self::trimValue($row[self::$reach_max]),
            'location' => self::trimValue($row[self::$location]),
        ];
    }

    private static function slotData(array $row)
    {
        return [
            'name' => self::trimValue($row[self::$name]),
            'price' => self::trimValue($row[self::$price]),
            'media_type' => self::trimValue($row[self::$media_type]),
        ];
    }
}
