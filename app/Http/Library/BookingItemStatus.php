<?php

namespace App\Http\Library;

class BookingItemStatus
{
    public static $APPROVED = 'approved';
    public static $REJECTED = 'rejected';
    public static $AWAITING = 'awaiting_response';
    public static $PUBLISHED = 'published';
    public static $SCHEDULED = 'scheduled';
    public static $NO_MATERIAL = 'no_material';
    public static $MATERIAL_REJECTED = 'material_rejected';

    public static function getActionsAsString()
    {
        return
            self::$APPROVED . ',' .
            self::$REJECTED . ',' .
            self::$AWAITING . ',' .
            self::$PUBLISHED . ',' .
            self::$SCHEDULED . ',' .
            self::$NO_MATERIAL . ',' .
            self::$MATERIAL_REJECTED;
    }
}
