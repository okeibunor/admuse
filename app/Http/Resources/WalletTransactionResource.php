<?php

namespace App\Http\Resources;

use App\WalletTransaction;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WalletTransactionResource extends JsonResource
{
    /**
     * @var WalletTransaction
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'amount' => $this->resource->amount,
            'type' => $this->resource->type,
            'date' => $this->resource->created_at->toDateTimeLocalString(),
            'note' => $this->resource->note
        ];
    }
}
