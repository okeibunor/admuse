<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class PaginatedResource extends ResourceCollection
{

    /**
     * @var string
     */
    private $className;

    /**
     * PaginatedResource constructor.
     * @param LengthAwarePaginator|\Illuminate\Contracts\Pagination\LengthAwarePaginator $resource
     * @param string $className
     */
    public function __construct(LengthAwarePaginator $resource, string $className)
    {
        parent::__construct($resource);

        $this->className = $className;
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $data LengthAwarePaginator
         */
        $data = $this->resource;

        return [
            'data' => $data->map(function ($item) {
                return new $this->className($item);
            }),
            'links' => [
                'prev_page' => $data->previousPageUrl(),
                'next_page' => $data->nextPageUrl(),
                'first_page' => $data->url(1),
                'last_page' => $data->url($data->lastPage()),
            ],
            'info' => [
                'total' => $data->total(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'per_page' => $data->perPage(),
                'to' => $data->lastItem(),
            ]];
    }
}
