<?php

namespace App\Http\Resources;

use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class WithdrawalResource extends JsonResource
{
    /**
     * @var WithdrawalRequest
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->resource->id,
            'amount' => $this->resource->amount,
            'status' => $this->resource->status,
            'created_at' => $this->resource->created_at->toDateTimeLocalString(),
        ];

        // we don't want to query the user db again
        if (getUser()->isAdmin()) {
            $data['user'] = new UserResource($this->resource->user);
            $data['processed_by'] = $this->resource->admin ? new UserResource($this->resource->admin) : null;
        }

        return $data;
    }
}
