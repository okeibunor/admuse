<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PublicUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        $user = $this->resource;
        return [
            'id' => $user->id,
            'name' => $user->name,
            'type' => $user->type->name,
            'business_name' => $user->business_name,
            'sector' => $user->sector,
            'website' => $user->website,
        ];
    }
}
