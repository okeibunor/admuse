<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => url($this->path),
            'mime' => $this->mime,
            'size' => $this->size,
            'created_at' => $this->created_at->toDateTimeLocalString(),
            'updated_at' => $this->updated_at->toDateTimeLocalString()
        ];
    }
}
