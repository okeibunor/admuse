<?php

namespace App\Http\Resources;

use App\Contact;
use App\Media;
use Illuminate\Http\Resources\Json\JsonResource;
use App\BookingSlot;

class MediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $publication Media
         */
        $publication = $this->resource;

        return [
            'name' => $publication->name,
            'description' => $publication->description,
            'short_name' => $publication->short_name,
            'address' => $publication->address,
            'user' => [
                'id' => $publication->user->id,
                'name' => $publication->user->name,
            ],
            'images' => [
                'avatar' => $publication->avatar ? env('APP_URL') . '/' . $publication->avatar : null
            ],
            'apcon_number' => $publication->apcon_number,
            'verified' => (bool)$publication->verified,
            'mobile' => $publication->mobile,
            'email' => $publication->email,
            'program_count' => $publication->program()->count(),
            'slot_count' => $publication->slots()->count(),
            'booking_count' => BookingSlot::mediaHouseBookedItems($publication->id)->count(),
            'created_at' => $publication->created_at->toDateTimeLocalString(),
            'updated_at' => $publication->created_at->toDateTimeLocalString()
        ];
    }
}
