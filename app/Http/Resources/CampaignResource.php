<?php

namespace App\Http\Resources;

use App\Campaign;
use App\CampaignItem;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * @var Campaign
     */
    public $resource;
    /**
     * @var bool
     */
    private $containItemList;

    public function __construct($resource, $containItemList = false)
    {
        $this->containItemList = $containItemList;

        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'description' => $this->resource->description,
            'location' => $this->resource->location,
            'budget' => $this->resource->budget,
            'paid' => (bool)$this->resource->paid,
            'cost' => $this->resource->getItemSum(),
            'social_class' => $this->resource->socialClass,
            'age_group' => $this->resource->ageGroup,
            'interests' => $this->resource->interests,
            'starts_at' => $this->resource->starts_at->toDateTimeLocalString(),
            'ends_at' => $this->resource->ends_at->toDateTimeLocalString(),
            'created_at' => $this->resource->created_at->toDateTimeLocalString(),
            'updated_at' => $this->resource->updated_at->toDateTimeLocalString(),
            'slot_count' => $this->resource->slots()->count()
        ];

        if ($this->containItemList) {
            $data['items'] = $this->resource->slots->map(function (CampaignItem $item) {
                return new CampaignItemResource($item);
            });
        }

        return $data;
    }
}
