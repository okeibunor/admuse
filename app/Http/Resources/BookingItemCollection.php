<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class BookingItemCollection extends ResourceCollection
{

    /**
     * @var Collection
     */
    public $resource;

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return Collection
     */
    public function toArray($request)
    {
        return $this->resource->map(function ($item) {
            return new BookingItemResource($item);
        });
    }
}
