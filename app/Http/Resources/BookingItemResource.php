<?php

namespace App\Http\Resources;

use App\BookingSlot;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingItemResource extends JsonResource
{

    /**
     * @var BookingSlot
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'status' => $this->resource->status,
            'note' => $this->resource->note,
            'media_house_note' => $this->resource->media_house_note,
            'materials' => new FileCollection($this->resource->material),
            'slot' => new SlotResource($this->resource->slot),
            'published_at' => !$this->resource->published_at ? null : $this->resource->published_at->toDateTimeLocalString(),
            'preferred_publication_date' => !$this->resource->preferred_publication_date ? null : $this->resource->preferred_publication_date->toDateTimeLocalString()
        ];
    }
}
