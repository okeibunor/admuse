<?php

namespace App\Http\Resources;

use App\File;
use App\Program;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProgramResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $program Program
         */
        $program = $this->resource;

        return [
            'name' => $program->name,
            'description' => $program->description,
            'address' => $program->address,
            'location' => $program->location,
            'slots' => $program->slot()->count(),
            'reach' => [
                'min' => $program->reach_min,
                'max' => $program->reach_max
            ],
            'images' => @$program->images ? $program->images->map(function (File $image) {
                return new FileResource($image);
            }) : null,
            'short_name' => $program->short_name,
            'media' => new MediaResource($program->media),
            'channel' => $program->channel,
            'sub_channel' => $program->subChannel,
            'type' => $program->type,
            'schedule' => $program->schedule,
            'social_class' => $program->socialClass,
            'literacy_level' => $program->literacyLevel,
            'interest' => $program->interest,
            'genders' => $program->genders,
            'age_group' => $program->ageGroup,
            'profession_level' => $program->professionLevel,
            'cost_factor' => $program->costFactor,
            'contacts' => $program->contacts,
            'marital_statuses' => $program->maritalStatus
        ];
    }
}
