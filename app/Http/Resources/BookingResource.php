<?php

namespace App\Http\Resources;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $booking Booking
         */
        $booking = $this->resource;

        return [
            'id' => $booking->id,
            'paid' => (boolean)$booking->paid,
            'amount' => $booking->getItemsSum(),
            'paid_amount' => $booking->amount_paid,
            'status' => $booking->status,
            'payment_method' => $booking->payment_method,
            'items' => new BookingItemCollection($booking->bookedItems),
            'created_at' => $booking->created_at->toDateTimeLocalString(),
            'updated_at' => $booking->updated_at->toDateTimeLocalString(),
        ];
    }
}
