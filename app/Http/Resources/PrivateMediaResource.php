<?php

namespace App\Http\Resources;

use App\Media;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PrivateMediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $publication Media
         */
        $publication = $this->resource;

        return [
            'name' => $publication->name,
            'description' => $publication->description,
            'short_name' => $publication->short_name,
            'address' => $publication->address,
            'user' => new UserResource($publication->user),
            'verified' => (bool)$publication->verified,
            'mobile' => $publication->mobile,
            'email' => $publication->email,
            'created_at' => $publication->created_at->toDateTimeLocalString(),
            'updated_at' => $publication->created_at->toDateTimeLocalString()
        ];
    }
}
