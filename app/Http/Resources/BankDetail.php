<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BankDetail extends JsonResource
{
    /**
     * @var \App\BankDetail
     */
    public $resource;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource->name,
            'bank_code' => $this->resource->bank_code,
            'bank_name' => $this->resource->bank_name,
            'number' => $this->resource->number
        ];
    }
}
