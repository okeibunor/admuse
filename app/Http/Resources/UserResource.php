<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $user User
         */
        $user = $this->resource;
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'type' => $user->type->name,
            'balance' => @$user->wallet->amount ? $user->wallet->amount : 0.00,
            'phone' => $user->phone,
            'business_name' => $user->business_name,
            'sector' => $user->sector,
            'address' => $user->address,
            'website' => $user->website,
            'products_and_services' => $user->products_and_services,
        ];
    }
}
