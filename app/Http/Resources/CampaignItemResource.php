<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CampaignItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'item_id' => $this->resource->id,
            'quantity' => $this->resource->quantity,
            'slots' => new SlotResource($this->resource->slot)
        ];
    }
}
