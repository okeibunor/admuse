<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProgramCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array|mixed
     */
    public function toArray($request)
    {
        return new PaginatedResource($this->resource, ProgramResource::class);
    }
}
