<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MediaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array|mixed
     */
    public function toArray($request)
    {
        return new PaginatedResource($this->resource, MediaResource::class);
    }
}
