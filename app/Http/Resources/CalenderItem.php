<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CalenderItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'status' => $this->resource->status,
            'note' => $this->resource->note,
            'media_house_note' => $this->resource->media_house_note,
            'published_at' => !$this->resource->published_at ? null : $this->resource->published_at->toDateTimeLocalString(),
        ];
    }
}
