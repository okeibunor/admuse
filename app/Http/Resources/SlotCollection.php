<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SlotCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|mixed
     */
    public function toArray($request)
    {
        return new PaginatedResource($this->resource, SlotResource::class);
    }
}
