<?php

namespace App\Http\Resources;

use App\File;
use App\Slot;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SlotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /**
         * @var $slot Slot
         */
        $slot = $this->resource;

        return [
            'id' => $slot->id,
            'name' => $slot->name,
            'price' => $slot->price,
            'available' => (bool)$slot->available,
            'size' => [
                'text' => $slot->size_value . $slot->size->size,
                'name' => $slot->size->size,
                'value' => $slot->size_value
            ],
            'images' => $slot->images->map(function (File $image) {
                return new FileResource($image);
            }),
            'program' => new ProgramResource($slot->program),
            'media_type' => $slot->mediaType->name
        ];
    }
}
