<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\Size;

class SizeController extends Controller
{
    public function getAll()
    {
        return tc(function () {
            $sizes = Size::paginate(10);

            if ($sizes->isEmpty()) return sendResponse('error', [
                'type' => 'No Data'
            ]);

            return sendSuccessResponse($sizes);
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                Size::firstOrCreate([
                    'size' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($size)
    {
        return tc(function () use ($size) {
            $size = Size::where('size', $size)->first();

            if (!$size) throw new Custom('Size Not Found');

            $size->size = $this->request->get('name', $size->name);
            $size->save();
            return sendSuccessResponse('Size Updated');
        });
    }

    public function delete($size)
    {
        return tc(function () use ($size) {
            Size::where('size', $size)->delete();
            return sendSuccessResponse('Size Deleted Successfully');
        });
    }
}
