<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\File;
use App\Http\Resources\FileResource;
use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
    public function delete()
    {
        return tc(function () {
            getRequestedFile()->delete();

            return sendSuccessResponse('File Removed Successfully');
        });
    }

    public function uploadImages()
    {
        return tc(function () {
            $this->validateUpload();
            $files = [];

            // since the images can be more than one, we
            // are going to loop through them
            foreach ($this->request->file('images') as $image) {
                $files[] = new FileResource(
                    $this::upload($image, getProgramUploadPath())
                );
            }

            return sendSuccessResponse($files);
        });
    }

    public static function upload(UploadedFile $file, $path = 'app')
    {
        try {
            $name = \Ramsey\Uuid\Uuid::uuid1()->toString() . '.' . $file->clientExtension();
        } catch (Exception $e) {
        }

        $file = $file->move(
            storage_path(
                $path
            ), @$name
        );

        return File::create([
            'path' => getProgramUploadPath() . $name,
            'mime' => $file->getMimeType(),
            'size' => $file->getSize(),
            'user_id' => getUser()->id,
            'object_id' => 0,
            'model' => ''
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function validateUpload()
    {
        $validator = Validator::make($this->request->all(), [
            'images' => 'required|array|min:1|max:3',
            'images.*' => 'file|mimes:jpg,png,jpeg|max:2048'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

}
