<?php

namespace App\Http\Controllers;

use App\AgeGroup;
use App\Exceptions\Custom;
use Illuminate\Http\Request;

class AgeGroupController extends Controller
{

    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                AgeGroup::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                AgeGroup::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($ageGroup)
    {
        return tc(function() use ($ageGroup) {
            $ageGroup = AgeGroup::where('name', $ageGroup)->first();

            if(!$ageGroup) throw new Custom('AgeGroup Not Found');

            $ageGroup->name = $this->request->get('name', $ageGroup->name);
            $ageGroup->save();
            return sendSuccessResponse('AgeGroup Updated');
        });
    }

    public function delete($ageGroup)
    {
        return tc(function() use ($ageGroup) {
            AgeGroup::where('name', $ageGroup)->delete();
            return sendSuccessResponse('AgeGroup Deleted Successfully');
        });
    }
}
