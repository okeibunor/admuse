<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Http\Resources\MediaResource;
use App\Media;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class ChannelController extends Controller
{
    public function list()
    {
        return tc(function () {
            // get all list of channels
            return sendSuccessResponse(Channel::all());
        });
    }

    public function create()
    {
        return tc(function () {
            generalValidation();

            Channel::create([
                'name' => $this->request->get('name')
            ]);
            return sendSuccessResponse('Channel Created Successfully');
        });
    }

    public function delete($channel)
    {
        return tc(function () use ($channel) {
            $channel = Channel::where('name', $channel)->delete();
            return sendSuccessResponse('Channel Deleted Successfully');
        });
    }

    public function edit($channel)
    {
        return tc(function () use ($channel) {
            $channel = Channel::where('name', $channel)->first();
            $channel->name = $this->request->get('name', $channel->name);
            $channel->save();

            return sendSuccessResponse('Channel Updated Successfully');
        });
    }

    public function getMediaList($channel)
    {
        return tc(function () use ($channel) {
            $channel = Channel::name($channel);
            $mediaList = Media::join('programs', 'programs.media_id', '=', 'media.id')
                ->select('media.*')
                ->groupBy('media.name', 'media.id', 'media.short_name'
                    , 'media.address', 'media.verified', 'media.mobile', 'media.updated_at'
                    , 'media.user_id', 'media.mission', 'media.created_at', 'media.description'
                    , 'media.vision', 'media.apcon_number', 'media.email')
                ->where('channel_id', $channel->id)
                ->get();

            return sendSuccessResponse(
                $mediaList->map(function (Media $media) {
                    return new MediaResource($media);
                })
            );
        });
    }
}
