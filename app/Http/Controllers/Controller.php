<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 * @property User $user
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var Request
     */
    public $request;

    /**
     * Authenticated User
     *
     * @var User|null
     */
    public $user;

    public function __construct()
    {
        /**
         * The only reason why we choose to call the constructor
         * is cause we need the tokenStatus helper function
         * to always be called.
         */
        if (!@$GLOBALS['user']) tokenStatus();

        $this->request = request();
        $this->user = $GLOBALS['user'];
    }
}
