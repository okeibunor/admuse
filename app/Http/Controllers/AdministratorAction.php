<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class AdministratorAction extends Controller
{
    public function __construct()
    {
        $this->middleware(['private','admin']);

        parent::__construct();
    }

    public function createNewUser(Request $request)
    {
        return tc(function() use ($request) {
            $rule = RegisterController::getCreateRule();

            unset($rule['type'][2]);

            RegisterController::useCreateValidator($request->all(),$rule);

            $user = User::createUser(
                $request->get('name'),
                $request->get('email'),
                $request->get('password'),
                $request->get('type')
            );
            event(new Registered($user));
            return sendSuccessResponse(new UserResource($user));
        });
    }
}
