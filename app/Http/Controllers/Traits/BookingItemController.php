<?php

namespace App\Http\Controllers\Traits;

use App\BookingSlot;
use App\Events\BookingItemApproved;
use App\Events\BookingItemMaterialRejected;
use App\Events\BookingItemPublished;
use App\Events\BookingItemRejected;
use App\Events\BookingItemScheduled;
use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\Http\Library\BookingItemStatus;
use App\Http\Resources\BookingItemResource;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;

trait BookingItemController
{
    public function edit(Request $request)
    {
        return tc(function () use ($request) {
            $bookingItem = getRequestedBookingItem();

            $this->validateBookingItemEdit($request);
            $this->uploadMaterials($request, $bookingItem);

            $bookingItem->update([
                'preferred_publication_date' => new \Illuminate\Support\Carbon($request->get('preferred_date')),
                'note' => $request->get('note'),
            ]);

            return sendSuccessResponse(new BookingItemResource($bookingItem));
        });

    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateBookingItemEdit(Request $request)
    {
        $validatorInstance = Validator::make($request->all(), [
            'material' => 'sometimes|file|mimes:jpg,png,jpeg,mkv,avi,3gp,mp4',
            'preferred_date' => 'sometimes|date|after_or_equal:today',
            'note' => 'nullable|min:1'
        ]);

        if ($validatorInstance->fails()) throw new ValidationException($validatorInstance);
    }

    /**
     * @param Request $request
     * @param BookingSlot $bookingSlot
     * @throws Exception
     */
    public function uploadMaterials(Request $request, BookingSlot $bookingSlot)
    {

        foreach ($request->files as $file) {
            /**
             * @var $file UploadedFile
             */
            $name = Uuid::uuid1()->toString() . '.' . $file->getClientOriginalExtension();

            $file = $file->move(
                storage_path(config('filesystems.material.upload')),
                $name
            );

            $bookingSlot->material()->create([
                'path' => config('filesystems.material.upload') . '/' . $name,
                'mime' => $file->getMimeType(),
                'size' => $file->getSize(),
                'user_id' => $this->user->id
            ]);
        }
    }

    public function getBookingInformation()
    {
        return tc(function () {
            return sendSuccessResponse(new BookingItemResource(getRequestedBookingItem()));
        });
    }

    /**
     * Respond to booking item
     *
     * @return mixed
     */
    public function bookingItemStatus()
    {
        return tc(function () {

            $this->validateBookingItemApproval();

            $bookingItem = getRequestedBookingItem();

            $bookingItem->update([
                'status' => $status = request()->route('status'),
                'media_house_note' => request()->get('note', $bookingItem->media_house_note),
                'published_at' => new \Illuminate\Support\Carbon(request()->get('publication_date', $bookingItem->published_at))
            ]);

            switch ($status) {
                case BookingItemStatus::$MATERIAL_REJECTED:
                    event(new BookingItemMaterialRejected($bookingItem));
                    break;
                case BookingItemStatus::$REJECTED:
                    event(new BookingItemRejected($bookingItem));
                    break;
                case BookingItemStatus::$PUBLISHED:
                    event(new BookingItemPublished($bookingItem));
                    break;
                case BookingItemStatus::$SCHEDULED:
                    event(new BookingItemScheduled($bookingItem));
                    break;
                default:
                    event(new BookingItemApproved($bookingItem));
                    break;
            }

            return sendSuccessResponse("Booking Status Updated");
        });
    }

    /**
     * @throws Custom
     */
    public function validateBookingItemApproval()
    {
        $validator = Validator::make([
            'status' => request()->route('status')
        ], [
            'status' => 'in:' . BookingItemStatus::getActionsAsString()
        ], [
            'status.in' => 'Status can either be any of the following ' . BookingItemStatus::getActionsAsString()
        ]);

        if ($validator->fails()) throw new Custom("The provided status is invalid");
    }
}
