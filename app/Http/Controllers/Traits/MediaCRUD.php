<?php

namespace App\Http\Controllers\Traits;

use App\Exceptions\Custom;
use App\Http\Resources\MediaCollection;
use App\Http\Resources\MediaResource;
use App\Http\Resources\PrivateMediaResource;
use App\Media;
use App\User;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use function foo\func;

/**
 * Trait MediaCRUD
 * @package App\Http\Controllers\Traits
 * @property User user
 * @property Request request
 */
trait MediaCRUD
{
    public function create(Request $request)
    {
        return tc(function () use ($request) {
            //making a check to determine if we need to skip check
            //of how many media has been created by a user
            if ($this->user->isNotAdmin()) {
                if ($this->user->media()->exists())
                    throw new Custom("Cannot Create Multiple Media Houses");
            }

            $data = $request->all();

            $this->validateCreate($data);

            $data = (object)$data;

            /**
             * @var $publication Media
             */
            $publication = $this->user->media()->create([
                'name' => $data->name,
                'description' => @$data->description,
                'apcon_number' => @$data->apcon_number,
                'short_name' => @$data->short_name ?: guessShortName($data->name),
                'address' => @$data->address,
                'mobile' => @$data->mobile,
                'email' => @$data->email,
                'vision' => @$data->vision,
                'mission' => @$data->mission,
            ]);
            return sendSuccessResponse(new MediaResource($publication));
        });
    }

    public function list()
    {
        return tc(function () {
            $houses = Media::paginate(15);

            return sendSuccessResponse(new MediaCollection($houses));
        });
    }

    public function information()
    {
        return tc(function () {
            return sendSuccessResponse(new MediaResource(getMediaHouse()));
        });
    }

    public function owned()
    {
        return tc(function () {

            $media = $this->user->media;
            if(!$media) throw new Custom('No Data');

            return sendSuccessResponse(new MediaResource($media));
        });
    }

    public function edit()
    {
        return tc(function () {
            $this->validateEdit($this->request->all());

            $media = getMediaHouse();
            $media->update(
                $this->request->only([
                    'name', 'address', 'mobile', 'email', 'apcon_number',
                    'vision', 'mission', 'description'
                ])
            );

            return sendSuccessResponse(new MediaResource(
                Media::where('short_name', $this->request->route('media_house'))->first()
            ));
        });
    }

    public function uploadAvatar()
    {
        return tc(function(){
            $this->validateAvatarUpload();
            $media = getMediaHouse();
            $file = $this->request->file('avatar');

            // generate unique name for file
            $name = Uuid::uuid1()->toString();
            // move uploaded file
            $file = moveMediaUpload($file, $name);
            //update avatar
            $media->avatar = getMediaAvatarUploadPath() . '/'. $file->getFilename();
            $media->save();

            return sendSuccessResponse('Media Avatar Uploaded');
        });
    }
}
