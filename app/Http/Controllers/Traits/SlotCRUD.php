<?php
/**
 * Created by PhpStorm.
 * User: lawaloladipupo
 * Date: 2019-08-23
 * Time: 18:58
 */

namespace App\Http\Controllers\Traits;

use App\File;
use App\Http\Resources\SlotCollection;
use App\Http\Resources\SlotResource;
use App\Slot;
use Illuminate\Http\Request;

trait SlotCRUD
{
    /**
     * @var Request
     */
    public $request;

    public function create()
    {
        return tc(function () {
            $this->createValidator();

            $program = getCurrentProgram();

            $slot = $program->slot()
                ->create(
                    array_merge(
                        [
                            'draft' => $this->request->get('draft', false),
                            'media_id' => $program->media_id,
                            'type_id' => $program->type->id
                        ],
                        request()->only(['name', 'price', 'available', 'media_type_id', 'size_id', 'size_value'])
                    )
                );
            foreach ($this->request->get('image') as $image) {
                File::assignToAnotherModel($image, $slot->id, Slot::class);
            }

            return new SlotResource($slot);
        });
    }

    public function edit()
    {
        return tc(function () {
            $this->createValidator();

            /**
             * @var Slot $slot
             */
            $slot = getRequestedSlot();

            $slot->draft = $this->request->get('draft', $slot->draft);
            $slot->name = $this->request->get('name');
            $slot->price = $this->request->get('price');
            $slot->type_id = $this->request->get('type_id');
            $slot->media_type_id = $this->request->get('media_type_id');
            $slot->available = $this->request->get('available');

            $slot->save();

            foreach ($this->request->get('image') as $image) {
                File::assignToAnotherModel($image, $slot->id, Slot::class);
            }

            return new SlotResource($slot);
        });
    }

    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                new SlotCollection(
                    Slot::getProgramPaginatedSlots(10)
                )
            );
        });
    }

    public function draft()
    {
        return tc(function () {
            return sendSuccessResponse(
                new SlotCollection(
                    Slot::getProgramPaginatedDraftSlots(15)
                )
            );
        });
    }

    public function fetch()
    {
        return sendSuccessResponse(
            new SlotCollection(
                Slot::notDraft()->paginate(10)
            )
        );
    }

    public function getByID()
    {
        return tc(function () {
            return sendSuccessResponse(new SlotResource(
                getRequestedSlot()
            ));
        });
    }

    public function search()
    {
        return tc(function () {
            // get the search query
            $searchQuery = $this->request->get('query', '');
            $queryBuilder = Slot::where('slots.name', 'LIKE', "%$searchQuery%")
                ->orWhere('programs.name', 'LIKE', "%$searchQuery%")
                ->join('programs', 'programs.id', '=', 'slots.program_id');
            // get the filters
            // get max price and min price if available
            if ($price_min = $this->request->get('price_min')) {
                $queryBuilder = $queryBuilder->where('price', '>=', $price_min);
            }
            if ($price_max = $this->request->get('price_max')) {
                $queryBuilder = $queryBuilder->where('price', '<=', $price_max);
            }
            // get media house
            if ($mediaIds = $this->request->get('media_house_ids', null)) {
                $queryBuilder = $queryBuilder->whereIn('programs.media_id', $mediaIds);
            }

            // get by channel
            if ($channelIds = $this->request->get('channel_ids')) {
                $queryBuilder = $queryBuilder->whereIn('channel_id', $channelIds);
            }
            // get by sub channel
            if ($subChannelIds = $this->request->get('sub_channel_ids')) {
                $queryBuilder = $queryBuilder->whereIn('sub_channel_id', $subChannelIds);
            }

            // get by reach
            if ($reach_min = $this->request->get('reach_min')) {
                $queryBuilder = $queryBuilder->where('reach_min', '>=', $reach_min);
            }
            if ($reach_max = $this->request->get('reach_max')) {
                $queryBuilder = $queryBuilder->where('reach_max', '<=', $reach_max);
            }
            // get the sort

            $queryBuilder = $queryBuilder->select('slots.*');
//            dd($queryBuilder->toSql(), $queryBuilder->getBindings());

            return sendSuccessResponse(
                new SlotCollection(
                    $queryBuilder->paginate(10)
                )
            );
        });
    }

    public function all()
    {
        return tc(function () {
            return sendSuccessResponse(
                new SlotCollection(
                    Slot::paginate(10)
                )
            );
        });
    }
}
