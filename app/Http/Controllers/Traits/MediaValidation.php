<?php

namespace App\Http\Controllers\Traits;

use App\Exceptions\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Trait MediaValidation
 * @package App\Http\Controllers\Traits
 * @property Request request
 */
trait MediaValidation
{
    /**
     * @param $data
     * @throws ValidationException
     */
    public function validateCreate($data)
    {
        $validator = Validator::make($data, [
            'name' => 'required|unique:media,name|min:3|max:190',
            'email' => 'nullable|email',
            'short_name' => 'nullable|unique:media',
            'contacts' => 'nullable|array|min:1',
            'contacts.*.name' => 'sometimes|required',
            'contacts.*.mobile' => 'sometimes|required',
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    /**
     * @param $data
     * @throws ValidationException
     */
    public function validateEdit($data)
    {
        $validator = Validator::make($data, [
            'name' => 'required|min:3|max:190',
            'email' => 'nullable|email',
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    /**
     * @param $data
     * @throws ValidationException
     */
    public function validateAvatarUpload()
    {
        $validator = Validator::make($this->request->all(), [
            'avatar' => 'required|file|max:2048|mimes:jpg,png,jpeg'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }
}
