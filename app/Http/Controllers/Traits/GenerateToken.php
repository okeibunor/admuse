<?php

namespace App\Http\Controllers\Traits;

use App\User;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;

trait GenerateToken
{

    /**
     * Generate token
     *
     * @param User $user
     * @return mixed
     **/
    public function generateToken(User $user)
    {
        $payload = ['exp' => Carbon::now()->addMonth(10)->timestamp];
        return JWTAuth::fromUser($user, $payload);
    }
}
