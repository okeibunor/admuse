<?php
/**
 * Created by PhpStorm.
 * User: lawaloladipupo
 * Date: 2019-08-28
 * Time: 07:11
 */

namespace App\Http\Controllers\Traits;

use App\Exceptions\ValidationException;
use App\Rules\SubChannel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait ProgramValidation
{
    /**
     * @param Request $request
     * @throws ValidationException
     */
    public function validateCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3:max:170',
            'short_name' => 'nullable|unique:programs',
            'reach_min' => 'required|int|lt:reach_max',
            'reach_max' => 'required|int|gt:reach_min',
            'channel_id' => 'required|exists:channels,id',
            'sub_channel_id' => ['required', new SubChannel],
            'type_id' => ['required', 'exists:types,id'],
            'schedule_id' => ['required', 'exists:schedules,id'],
            'social_class' => ['required', 'array', 'min:1'],
            'social_class.*' => ['exists:social_classes,id'],
            'literacy_level' => ['required', 'array', 'min:1'],
            'literacy_level.*' => ['exists:literacy_levels,id'],
            'interest' => ['required', 'array', 'min:1'],
            'interest.*' => ['exists:interests,id'],
            'age_group' => ['required', 'array', 'min:1'],
            'age_group.*' => ['exists:age_groups,id'],
            'profession_level' => ['required', 'array', 'min:1'],
            'profession_level.*' => ['exists:profession_levels,id'],
            'cost_factor_id' => ['required', 'exists:cost_factors,id'],
            'image' => ['required', 'min:1', 'array'],
            'image.*' => ['exists:files,id'],
            'contacts' => ['required','array','min:1'],
            'contacts.*' => ['exists:contact,id'],
            'genders' => ['required','array','min:1'],
            'genders.*' => ['exists:genders,id'],
            'marital_status' => ['required','array','min:1'],
            'marital_status.*' => ['exists:marital_statuses,id']
        ], [
            'reach_min.lt' => 'The minimum reach must be lesser than maximum reach',
            'reach_max.gt' => 'The maximum reach must be greater than minimum reach',
        ]);

        if($validator->fails()) throw new ValidationException($validator);
    }
}
