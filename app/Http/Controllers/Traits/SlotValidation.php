<?php

namespace App\Http\Controllers\Traits;

use App\Exceptions\ValidationException;
use Illuminate\Support\Facades\Validator;

trait SlotValidation
{
    /**
     * @throws ValidationException
     */
    public function createValidator()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|min:2:max:170',
            'price' => 'required|int|min:10',
            'available' => 'required|bool',
            'media_type_id' => 'required|exists:media_types,id',
            'image' => 'required|array|min:1',
            'image.*' => ['exists:files,id'],
            'size_value' => 'required|min:1',
            'size_id' => 'required|int|exists:sizes,id'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }
}
