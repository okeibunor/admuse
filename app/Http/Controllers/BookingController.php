<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingSlot;
use App\Events\BookingSaved;
use App\Exceptions\Custom;
use App\Http\Controllers\Traits\BookingItemController;
use App\Http\Library\BookingStatus;
use App\Http\Library\Cart;
use App\Http\Resources\BookingCollection;
use App\Http\Resources\BookingItemCollection;
use App\Http\Resources\BookingResource;
use App\Http\Library\PayStack;
use Illuminate\Http\Response;

class BookingController extends Controller
{

    use BookingItemController;

    public function create()
    {
        return tc(function () {
            $availableItems = Cart::get();

            /**
             * @var $booking Booking
             */
            $booking = getUser()->booking()->create([]);

            foreach ($availableItems as $item) {
                BookingSlot::create([
                    'slot_id' => $item->id,
                    'booking_id' => $booking->id
                ]);
            }

            event(new BookingSaved($booking));

            return sendSuccessResponse("Items Booked Successfully");
        });
    }

    public function list()
    {
        return tc(function () {
            $bookings = getUser()->booking()->latest()->paginate(15);

            if ($bookings->isEmpty()) throw new Custom("No Data Available");

            return new BookingCollection($bookings);
        });
    }

    public function single()
    {
        return tc(function () {
            return sendSuccessResponse(new BookingResource(getCurrentBooking()));
        });
    }

    public function getMediaHouseBookings()
    {
        return tc(function () {

            $items = BookingSlot::mediaHouseBookedItems(getMediaHouse()->id)->latest('bookings.created_at')->paginate(10);

            if ($items->isEmpty()) return sendNoDataResponse();

            return sendSuccessResponse(new BookingItemCollection($items));
        });
    }

    public function payFromWallet()
    {
        return tc(function () {
            $booking = getCurrentBooking();

            // let's be sure that the user haven't
            // paid for this sh*t before

            if($booking->paid) throw new Custom('Booking Already Paid For');

            // check if the user has almost the amount
            // needed to proceed with payment
            $amount = $booking->getItemsSum();
            $this->user->checkUserBalance($amount);

            // debit the user
            $this->user->wallet->debit($amount, 'Payment for booking');

            $booking->update([
                'paid' => true,
                'amount' => $amount,
                'payment_method' => 'wallet',
                'amount_paid' => $amount,
                'status' => BookingStatus::$PROCESSING
            ]);
            return sendSuccessResponse("Payment Processed");
        });
    }

    public function payFromCard()
    {
        return tc(function(){
            $booking = getCurrentBooking();

            // check if booking is paid or not
            if($booking->paid) throw new Custom('Booking Already Paid For', Response::HTTP_ACCEPTED);
            // get the amount of the booking
            $amount = $booking->getItemsSum();

            $payment = PayStack::initTransaction(
                $amount,
                $this->user,
                ['booking_id' => $booking->id],
                route('verifyBookingPaymentByCard')
            );

            $booking->update([
                'amount' => $amount
            ]);

            return sendSuccessResponse([
                'url' => $payment->data->authorization_url,
                'total_amount' => PayStack::getAmountWithTransactionFee($amount),
                'amount' => $amount,
                'transaction_fee' => PayStack::getTransactionFee($amount)
            ]);
        });
    }

    public function verifyBookingPaymentByCard()
    {
        return tc(function(){
            $reference = $this->request->get('reference');

            $transactionInfo = PayStack::transactionInfo($reference);

            $booking = Booking::where('id', $transactionInfo->data->metadata->booking_id)->first();
            $booking->update([
                'amount_paid' => $transactionInfo->data->amount / 100,
                'paid' => true,
                'payment_method' => 'card',
                'status' => BookingStatus::$PROCESSING
            ]);

            return redirect(env('FRONTEND_HOST', 'https://test.hypedude.com'));
        });
    }

}
