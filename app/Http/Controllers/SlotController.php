<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\SlotCRUD;
use App\Http\Controllers\Traits\SlotValidation;
use App\Http\Library\CSVImport;
use App\MediaType;

class SlotController extends Controller
{
    use SlotCRUD, SlotValidation;

    public function bulk()
    {
        return tc(
            function () {
                $count = 0;
                $program = getCurrentProgram();

                foreach (CSVImport::getEachLine('setSlotIndex', 'slotData') as $data) {
                    $media_type_id = MediaType::{$data['media_type']}();
                    unset($data['media_type']);
                    $program->slot()->create(
                        array_merge($data, [
                            'type_id' => 1,
                            'size_id' => 1,
                            'size_value' => 1,
                            'media_id' => $program->media_id,
                            'draft' => false,
                            'media_type_id' => $media_type_id
                        ])
                    );
                    $count++;
                }

                return sendSuccessResponse(['processed' => $count]);
            });
    }

}
