<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Media;
use App\Program;
use App\Slot;

class InsightController extends Controller
{
    public function dashboard()
    {
        return tc(function () {
            $slots = Slot::count();
            $programs = Program::count();
            $medias = Media::count();
            $orders = Booking::count();

            return sendSuccessResponse(compact('slots', 'programs', 'medias', 'orders'));
        });
    }
}
