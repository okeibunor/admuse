<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Http\Resources\TypeResource;
use App\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TypeController extends Controller
{
    public function create(Request $request)
    {
        return tc(function () use ($request) {

            $this->validateCreate($request->all());

            Type::create([
                'name' => $request->get('name')
            ]);

            return sendSuccessResponse("Type Created Successfully");
        });
    }

    /**
     * @param array $all
     * @throws ValidationException
     */
    private function validateCreate(array $all)
    {
        $validator = Validator::make($all, [
            'name' => 'required|min:2|max:190|unique:types,name'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    public function list()
    {
        return sendSuccessResponse(
            Type::all()->map(function (Type $type) {
                return new TypeResource($type);
            })
        );
    }

    public function delete()
    {
        return tc(function () {
            Type::where('name', $this->request->route('type_name'))->delete();
            return sendSuccessResponse('Deleted Successfully');
        });
    }

    public function edit()
    {
        return tc(function () {
            $type = Type::where('name', $this->request->route('type_name'))->first();
            $type->update($this->request->only(['name']));
            return sendSuccessResponse('Updated Successfully');
        });
    }
}
