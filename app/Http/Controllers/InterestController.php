<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\Interest;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                Interest::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                Interest::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($interest)
    {
        return tc(function() use ($interest) {
            $interest = Interest::where('name', $interest)->first();

            if(!$interest) throw new Custom('Interest Not Found');

            $interest->name = $this->request->get('name', $interest->name);
            $interest->save();
            return sendSuccessResponse('Interest Updated');
        });
    }

    public function delete($interest)
    {
        return tc(function() use ($interest) {
            Interest::where('name', $interest)->delete();
            return sendSuccessResponse('Interest Deleted Successfully');
        });
    }
}
