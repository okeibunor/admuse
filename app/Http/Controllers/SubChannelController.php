<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Exceptions\Custom;
use App\SubChannel;
use Illuminate\Http\Request;

class SubChannelController extends Controller
{
    public function list($channel)
    {
        return tc(function () use ($channel) {
            // get the channel
            $channelInstance = Channel::where('name', $channel)->first();
            if (!$channelInstance) throw new Custom("Channel Not Found");
            // get the sub channels
            $subChannels = SubChannel::where('channel_id', $channelInstance->id)->get();
            return sendSuccessResponse($subChannels);
        });
    }

    public function create()
    {
        return tc(function(){
            generalValidation();
            // get the channel
            $channel = Channel::name($this->request->route('channel'));
            $channel->subChannel()->create([
                'name' => $this->request->get('name')
            ]);
            return sendSuccessResponse('Sub Channel Created Successfully');
        });
    }

    public function delete()
    {
        return tc(function () {
            SubChannel::where('name', $this->request->route('subchannel'))->delete();
            return sendSuccessResponse('Sub Channel Deleted Successfully');
        });
    }

    public function edit()
    {
        return tc(function () {
            $subChannel = SubChannel::where('name', $this->request->route('subchannel'))->first();
            $subChannel->name = $this->request->get('name', $subChannel->name);
            $subChannel->save();

            return sendSuccessResponse('Sub Channel Updated Successfully');
        });
    }
}
