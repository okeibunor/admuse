<?php

namespace App\Http\Controllers;

use App\BookingSlot;
use App\Http\Resources\CalenderItem;

class CalendarController extends Controller
{
    public function getEventsForThisMonth()
    {
        return tc(function () {
            $month = $this->request->get('month', now()->month);
            $year = $this->request->get('year', now()->year);

            $bookingItems = BookingSlot::whereRaw('Month(published_at) = ' . $month)
                ->whereRaw('Year(published_at) = ' . $year);

            if ($this->user->isNotPublisher()) {
                $bookingItems = $bookingItems->join('bookings', 'bookings.id', '=', 'booking_slots.booking_id')
                    ->where('bookings.user_id', $this->user->id);
            } else {
                $bookingItems = $bookingItems->join('slots', 'slots.id', '=', 'booking_slots.slot_id')
                    ->where('slots.media_id', $this->user->media->id);
            }

            // get scheduled posts
            return sendSuccessResponse(
                $bookingItems->get()->map(function (BookingSlot $slot) {
                    return new CalenderItem($slot);
                })
            );
        });
    }
}
