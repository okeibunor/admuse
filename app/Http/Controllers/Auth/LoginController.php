<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\GenerateToken;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    use GenerateToken;

    /**
     * @var Request
     */
    public $request;

    function __construct()
    {
        parent::__construct();
        $this->request = request();
    }

    function logIn()
    {
        try {

            $user = User::where("email", $this->request->email)->first();

            if ($user) {
                if (!password_verify($this->request->password, $user->password)) {
                    return $this->credentialError();
                }

                return sendResponse("success", [
                    "user" => new UserResource($user),
                    "token" => $this->generateToken($user)
                ]);
            } else {
                return $this->credentialError();
            }
        } catch (JWTException $e) {
            return sendResponse("error", ["message" => "Login Error!"], 400);
        }
    }

    public function credentialError()
    {
        return sendResponse("error", ["message" => "Invalid Credentials!"], 401);
    }
}
