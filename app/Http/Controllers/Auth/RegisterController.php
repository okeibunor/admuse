<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\ValidationException;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\GenerateToken;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use GenerateToken;

    private static $CREATE_RULE = [
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8'],
        'type' => ['required', 'exists:account_types,name', 'not_in:administrator']
    ];

    /**
     * @param array $data
     * @param $rule null|array
     * @return void
     * @throws ValidationException
     */
    public static function useCreateValidator(array $data, $rule)
    {
        (new self())->validator($data, $rule);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @param  array|null $rule
     * @throws ValidationException
     */
    protected function validator(array $data, $rule = null)
    {
        $validator = Validator::make($data, $rule ? $rule : self::$CREATE_RULE);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    public static function getCreateRule()
    {
        return self::$CREATE_RULE;
    }

    public function register(Request $request)
    {
        return tc(function () use ($request) {
            $this->validator($request->all());

            $user = $this->create($request->only(['name', 'email', 'password', 'type']));

            event(new Registered($user));

            return sendSuccessResponse([
                'token' => $this->generateToken($user),
                'user' => new UserResource($user)
            ]);
        });
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data = (object)$data;

        return User::createUser(
            $data->name,
            $data->email,
            Hash::make($data->password),
            $data->type);
    }
}
