<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\Http\Library\PayStack;
use App\Http\Resources\BankDetail;
use Illuminate\Support\Facades\Validator;

class BankDetailController extends Controller
{
    public function create()
    {
        return tc(function () {
            if ($this->user->bankDetail)
                throw new Custom("Information already created");

            $this->validateCreate();

            $recipient = PayStack::createTransferRecipient(
                $this->request->get('name'),
                $this->request->get('number'),
                $this->request->get('bank_code')
            );

            $bankDetail = $this->user->bankDetail()->create([
                'number' => $this->request->get('number'),
                'bank_name' => $recipient->data->details->bank_name,
                'bank_code' => $this->request->get('bank_code'),
                'name' => $recipient->data->name,
                'recipient_code' => $recipient->data->recipient_code
            ]);

            return sendSuccessResponse(
                new BankDetail($bankDetail)
            );
        });
    }

    /**
     * @throws ValidationException
     */
    public function validateCreate()
    {
        $validator = Validator::make($this->request->all(), [
            'number' => 'required|int',
            'bank_code' => 'required',
            'name' => 'required'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    public function info()
    {
        return tc(function () {
            $detail = $this->user->bankDetail;

            if (!$detail) throw new Custom('Bank Details Not Created');

            return sendSuccessResponse(
                new BankDetail($detail)
            );
        });
    }

    public function getUserBankName()
    {
        return tc(function () {
            return sendSuccessResponse(
                PayStack::_getAccountName(
                    $this->request->get('number'),
                    $this->request->get('bank_code')
                )->data
            );
        });
    }
}
