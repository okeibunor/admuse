<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\SocialClass;
use Illuminate\Http\Request;

class SocialClassController extends Controller
{
    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                SocialClass::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                SocialClass::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($socialClass)
    {
        return tc(function() use ($socialClass) {
            $socialClass = SocialClass::where('name', $socialClass)->first();

            if(!$socialClass) throw new Custom('SocialClass Not Found');

            $socialClass->name = $this->request->get('name', $socialClass->name);
            $socialClass->save();
            return sendSuccessResponse('SocialClass Updated');
        });
    }

    public function delete($socialClass)
    {
        return tc(function() use ($socialClass) {
            SocialClass::where('name', $socialClass)->delete();
            return sendSuccessResponse('SocialClass Deleted Successfully');
        });
    }
}
