<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\ProfessionLevel;
use Illuminate\Http\Request;

class ProfessionalLevelController extends Controller
{

    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                ProfessionLevel::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                ProfessionLevel::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($professionLevel)
    {
        return tc(function() use ($professionLevel) {
            $professionLevel = ProfessionLevel::where('name', $professionLevel)->first();

            if(!$professionLevel) throw new Custom('ProfessionLevel Not Found');

            $professionLevel->name = $this->request->get('name', $professionLevel->name);
            $professionLevel->save();
            return sendSuccessResponse('ProfessionLevel Updated');
        });
    }

    public function delete($professionLevel)
    {
        return tc(function() use ($professionLevel) {
            ProfessionLevel::where('name', $professionLevel)->delete();
            return sendSuccessResponse('ProfessionLevel Deleted Successfully');
        });
    }
}
