<?php

namespace App\Http\Controllers;

use App\Gender;
use App\Exceptions\ValidationException;
use App\Http\Resources\GenderResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GenderController extends Controller
{
    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                Gender::all()->map(function (Gender $gender) {
                    return new GenderResource($gender);
                })
            );
        });
    }
    public function create(Request $request)
    {
        // return tc(function () use ($request) {
            $this->validateCreate($request->all());

            Gender::create([
                'name' => $request->get('name')
            ]);

            return sendSuccessResponse("Gender Created Successfully");
        // });
    }

    public function delete()
    {
        return tc(function () {
            Gender::where('name', $this->request->route('type_name'))->delete();
            return sendSuccessResponse('Deleted Successfully');
        });
    }

    public function edit()
    {
        return tc(function () {
            $type = Gender::where('name', $this->request->route('type_name'))->first();
            $type->update($this->request->only(['name']));
            return sendSuccessResponse('Updated Successfully');
        });
    }
    /**
     * @param array $all
     * @throws ValidationException
     */
    private function validateCreate(array $all)
    {
        $validator = Validator::make($all, [
            'name' => 'required|min:2|max:190|unique:genders,name'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }
}
