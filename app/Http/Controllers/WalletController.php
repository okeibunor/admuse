<?php

namespace App\Http\Controllers;

use App\Http\Library\PayStack;
use App\Http\Resources\WalletTransactionCollection;
use App\Wallet;
use App\WalletTransaction;

class WalletController extends Controller
{
    public function getTransactionHistory()
    {
        $user = getUser();

        return sendSuccessResponse(new WalletTransactionCollection(
            WalletTransaction::where('user_id', $user->id)->latest()->paginate(10)
        ));
    }

    public function fund()
    {
        return tc(function () {
            $amount = $this->request->get('amount');
            // initialize a transaction
            $payment = PayStack::initTransaction(
                $amount,
                $this->user,
                ['actual_amount' => $amount]
            );

            // return information of account
            return sendSuccessResponse([
                'url' => $payment->data->authorization_url,
                'total_amount' => PayStack::getAmountWithTransactionFee($amount),
                'amount' => $amount,
                'transaction_fee' => PayStack::getTransactionFee($amount)
            ]);
        });
    }

    public function claim()
    {
        return tc(function () {
            $transaction = PayStack::transactionInfo(
                $reference = $this->request->route('reference')
            );

            // verify the id of the user since it doesn't change
            if ($transaction->data->metadata->id !== $this->user->id)
                return sendResponse('error', [
                    'type' => 'Access Denied',
                    'message' => 'Transaction belongs to another user!'
                ], 401);

            // check if the transaction was claimed
            if(WalletTransaction::getByReference($reference)){
                return sendResponse('error',[
                    'type' => 'Access Denied',
                    'message' => 'Transaction Verified Already',
                ], 401);
            }

            // get the user and then credit them
            $this->user->wallet->credit(
                $transaction->data->metadata->actual_amount,
                'Credit: Wallet Funded ' . now()->toDateTimeLocalString(),
                $reference
            );

            return sendSuccessResponse('Transaction Claimed');
        });
    }
}
