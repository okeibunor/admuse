<?php

namespace App\Http\Controllers;

use App\Events\WalletWithdrawApproval;
use App\Events\WalletWithdrawRejected;
use App\Events\WalletWithdrawRequest;
use App\Exceptions\Custom;
use App\Http\Resources\PaginatedResource;
use App\Http\Resources\WithdrawalResource;
use App\WithdrawalRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WithdrawalRequestController extends Controller
{
    /**
     * @var null|WithdrawalRequest
     */
    private $withdrawalRequest = null;

    public function withdraw()
    {
        return tc(function () {
            $amount = $this->request->get('amount', 1000);
            // get the amount requesting
            $this->user->checkUserBalance($amount);
            // create a withdrawal request
            event(new WalletWithdrawRequest(
                $this->user->withdraw($amount)
            ));
            return sendSuccessResponse('Withdrawal Request Sent');
        });
    }

    public function list()
    {
        return tc(function () {

            // get the type of user to determine which data will be sent
            if ($this->user->isAdmin()) $data = WithdrawalRequest::latest()->paginate(15);
            else $data = $this->user->withdrawals()->latest()->paginate(15);

            return sendSuccessResponse(
                new PaginatedResource(
                    $data,
                    WithdrawalResource::class
                )
            );
        });
    }

    public function respond()
    {
        return tc(function () {
            $action = strtolower($this->request->get('action', 'reject'));

            $this->getWithdrawalRequest();

            if ($action === 'reject') $this->rejectWithdrawRequest();
            else $this->approveWithdrawRequest();

            return sendSuccessResponse('Response Recorded');
        });
    }

    /**
     * @return WithdrawalRequest|Builder|Model|object|null
     * @throws Custom
     */
    public function getWithdrawalRequest()
    {
        if ($this->withdrawalRequest) return $this->withdrawalRequest;

        /**
         * @var WithdrawalRequest
         */
        $withdrawalRequest = WithdrawalRequest::where('id', $this->request->get('id'))->first();

        if (!$withdrawalRequest) throw new Custom('Invalid Withdrawal Request ID');

        if ($withdrawalRequest->status !== 'waiting') throw new Custom('Withdrawal Already Has Response');

        return $this->withdrawalRequest = $withdrawalRequest;
    }

    public function rejectWithdrawRequest()
    {
        $this->withdrawalRequest->status = 'rejected';
        $this->withdrawalRequest->admin_id = $this->user->id;
        $this->withdrawalRequest->save();

        event(new WalletWithdrawRejected($this->withdrawalRequest));
    }

    public function approveWithdrawRequest()
    {
        $this->withdrawalRequest->status = 'approved';
        $this->withdrawalRequest->admin_id = $this->user->id;
        $this->withdrawalRequest->save();
        event(new WalletWithdrawApproval($this->withdrawalRequest));
    }
}
