<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\MediaCRUD;
use App\Http\Controllers\Traits\MediaValidation;
use App\Http\Library\CSVImport;
use App\Http\Resources\PaginatedResource;
use App\Http\Resources\SlotResource;

class MediaController extends Controller
{
    use MediaCRUD, MediaValidation;

    public function importMediaHouse()
    {
        return tc(function () {
            $count = 0;

            foreach (CSVImport::getEachLine('setMediaHouseIndex', 'provideMediaHouseData') as $data) {
                $this->user->media()->create(array_merge($data, [
                    'short_name' => guessShortName($data['name'])
                ]));
                $count++;
            }

            return sendSuccessResponse(['processed' => $count]);
        });
    }

    public function fetchSlotList()
    {
        return tc(function() {
            return sendSuccessResponse(
                new PaginatedResource(
                    getMediaHouse()->slots()->paginate(10),
                    SlotResource::class
                )
            );
        });
    }
}
