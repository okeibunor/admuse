<?php

namespace App\Http\Controllers;

use App\CostFactor;
use App\Exceptions\Custom;
use Illuminate\Http\Request;

class CostFactorController extends Controller
{

    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                CostFactor::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                CostFactor::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($costFactor)
    {
        return tc(function () use ($costFactor) {
            $costFactor = CostFactor::where('name', $costFactor)->first();

            if (!$costFactor) throw new Custom('CostFactor Not Found');

            $costFactor->name = $this->request->get('name', $costFactor->name);
            $costFactor->save();
            return sendSuccessResponse('CostFactor Updated');
        });
    }

    public function delete($costFactor)
    {
        return tc(function () use ($costFactor) {
            CostFactor::where('name', $costFactor)->delete();
            return sendSuccessResponse('CostFactor Deleted Successfully');
        });
    }
}
