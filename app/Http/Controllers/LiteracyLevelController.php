<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\LiteracyLevel;
use Illuminate\Http\Request;

class LiteracyLevelController extends Controller
{
    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                LiteracyLevel::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                LiteracyLevel::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($literacyLevel)
    {
        return tc(function() use ($literacyLevel) {
            $literacyLevel = LiteracyLevel::where('name', $literacyLevel)->first();

            if(!$literacyLevel) throw new Custom('LiteracyLevel Not Found');

            $literacyLevel->name = $this->request->get('name', $literacyLevel->name);
            $literacyLevel->save();
            return sendSuccessResponse('LiteracyLevel Updated');
        });
    }

    public function delete($literacyLevel)
    {
        return tc(function() use ($literacyLevel) {
            LiteracyLevel::where('name', $literacyLevel)->delete();
            return sendSuccessResponse('LiteracyLevel Deleted Successfully');
        });
    }

}
