<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\CampaignItem;
use App\Exceptions\Custom;
use App\Http\Resources\CampaignItemResource;
use Illuminate\Http\Response;

class CampaignItemController extends Controller
{
    public function create()
    {
        return tc(function () {
            /**
             * @var Campaign $campaign
             */
            $campaign = getRequestedCampaign();

            if($campaign->hasSlot(getRequestedSlot()->id))
                throw new Custom('Slot Already In Campaign', Response::HTTP_BAD_REQUEST);

            $item = $campaign->items()->create([
                'slot_id' => getRequestedSlot()->id,
                'quantity' => $this->request->get('quantity', 1)
            ]);

            return sendSuccessResponse(new CampaignItemResource($item));
        });
    }

    public function update()
    {
        return tc(function () {
            /**
             * @var CampaignItem $campaign
             */
            $campaign = getRequestedCampaign();
            $campaign->quantity = $this->request->get('quantity', $campaign->quantity);
            $campaign->save();

            return sendSuccessResponse(new CampaignItemResource($campaign));
        });
    }

    public function remove()
    {
        return tc(function () {
            getRequestedCampaignItem()->delete();
            return sendSuccessResponse('Booking Removed');
        });
    }
}
