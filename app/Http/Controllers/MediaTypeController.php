<?php

namespace App\Http\Controllers;

use App\MediaType;

class MediaTypeController extends Controller
{
    public function all()
    {
        return tc(function () {
            return sendSuccessResponse(
                MediaType::all()
            );
        });
    }
}
