<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\Http\Library\PayStack;
use App\Http\Resources\CampaignResource;
use App\Http\Resources\PaginatedResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class CampaignController extends Controller
{
    public function index()
    {
        return tc(function () {
            $campaigns = $this->user->campaign()->paginate(15);

            if ($campaigns->isEmpty()) {
                throw new Custom('No Data', 404);
            }

            return sendSuccessResponse(new PaginatedResource($campaigns, CampaignResource::class));
        });
    }

    public function store()
    {
        return tc(function () {
            $this->validateNew();

            /**
             * @var Campaign $campaign
             */
            $campaign = $this->user->campaign()->create(
                $this->request->only('name', 'description', 'location', 'budget', 'starts_at', 'ends_at')
            );

            $campaign->saveRelateables($this->request);

            return sendSuccessResponse(new CampaignResource($campaign));
        });
    }

    /**
     * @throws ValidationException
     */
    public function validateNew()
    {
        $validator = Validator::make($this->request->all(), [
            'name' => 'required|min:2',
            'description' => 'required|min:2',
            'location' => 'required|min:2',
            'social_class' => ['required', 'array', 'min:1'],
            'social_class.*' => ['exists:social_classes,id'],
            'budget' => 'required|int',
            'interest' => ['required', 'array', 'min:1'],
            'interest.*' => ['exists:interests,id'],
            'starts_at' => 'required|date|after:today',
            'ends_at' => 'required|date|after:starts_at',
            'age_group' => ['required', 'array', 'min:1'],
            'age_group.*' => ['exists:age_groups,id'],
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    public function show()
    {
        return tc(function () {
            $campaign = getRequestedCampaign();

            return sendSuccessResponse(
                new CampaignResource($campaign, true)
            );
        });
    }

    public function destroy()
    {
        return tc(function () {
            $campaign = getRequestedCampaign();
            $campaign->delete();
            return sendSuccessResponse('Campaign Deleted');
        });
    }

    public function payWithWallet()
    {
        // todo: ask Chucks who sets or what determines the a completed campaign
        return tc(function () {
            /**
             * @var Campaign $campaign
             */
            $campaign = getRequestedCampaign();

            if ($campaign->paid) throw new Custom('Campaign Already Paid', Response::HTTP_NOT_ACCEPTABLE);

            $amount = $campaign->getItemSum();
            $this->user->checkUserBalance($amount);

            $this->user->wallet->debit($amount, 'Campaign Payment: \'' . $campaign->name . '\'');

            $campaign->amount = $amount;
            $campaign->paid = true;
            $campaign->save();

            return sendSuccessResponse('Campaign Payment Received');
        });
    }


    public function payWithCard()
    {
        return tc(function () {
            $campaign = getRequestedCampaign();

            if ($campaign->paid) throw new Custom('Campaign Already Paid For', Response::HTTP_ACCEPTED);

            $amount = $campaign->getItemSum();

            $payment = PayStack::initTransaction($amount, $this->user, [
                'campaign_id' => $campaign->id
            ], route('verifyCampaignCardPayment'));

            $campaign->update([
                'amount' => $amount
            ]);

            return sendSuccessResponse([
                'url' => $payment->data->authorization_url,
                'total_amount' => PayStack::getAmountWithTransactionFee($amount),
                'amount' => $amount,
                'transaction_fee' => PayStack::getTransactionFee($amount)
            ]);
        });
    }


    public function verifyCardPayment()
    {
        return tc(function () {
            $reference = $this->request->get('reference');

            $transactionInfo = PayStack::transactionInfo($reference);

            $campaign = Campaign::where('id', $transactionInfo->data->metadata->campaign_id)->first();
            $campaign->update([
                'amount_paid' => $transactionInfo->data->amount / 100,
                'paid' => true,
            ]);

            return redirect(env('FRONTEND_HOST', 'https://test.hypedude.com'));
        });
    }
}
