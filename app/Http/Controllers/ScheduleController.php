<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\Interest;
use App\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function list()
    {
        return tc(function (){
            return sendSuccessResponse(
                Schedule::all()
            );
        });
    }

    public function create()
    {
        return tc(function () {
            return sendSuccessResponse(
                Schedule::firstOrCreate([
                    'name' => $this->request->get('name')
                ])
            );
        });
    }

    public function edit($schedule)
    {
        return tc(function() use ($schedule) {
            $schedule = Schedule::where('name', $schedule)->first();

            if(!$schedule) throw new Custom('Schdeule Not Found');

            $schedule->name = $this->request->get('name', $schedule->name);
            $schedule->save();
            return sendSuccessResponse('Schedule Updated');
        });
    }

    public function delete($interest)
    {
        return tc(function() use ($interest) {
            Schedule::where('name', $interest)->delete();
            return sendSuccessResponse('Schedule Deleted Successfully');
        });
    }
}
