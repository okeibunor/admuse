<?php

namespace App\Http\Controllers;

use App\Exceptions\ValidationException;
use App\Http\Resources\MaritalResource;
use App\MaritalStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MaritalController extends Controller
{
    public function list()
    {
        return tc(function () {
            return sendSuccessResponse(
                MaritalStatus::all()->map(
                    function (MaritalStatus $status) {
                        return new MaritalResource($status);
                    }
                )
            );
        });
    }

    public function create(Request $request)
    {
        return tc(function () use ($request) {

            $this->validateCreate($request->all());

            MaritalStatus::create([
                'name' => $request->get('name')
            ]);

            return sendSuccessResponse("MaritalStatus Created Successfully");
        });
    }

    public function delete()
    {
        return tc(function () {
            MaritalStatus::where('name', $this->request->route('type_name'))->delete();
            return sendSuccessResponse('Deleted Successfully');
        });
    }

    public function edit()
    {
        return tc(function () {
            $type = MaritalStatus::where('name', $this->request->route('type_name'))->first();
            $type->update($this->request->only(['name']));
            return sendSuccessResponse('Updated Successfully');
        });
    }
    /**
     * @param array $all
     * @throws ValidationException
     */
    private function validateCreate(array $all)
    {
        $validator = Validator::make($all, [
            'name' => 'required|min:2|max:190|unique:types,name'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }
}

