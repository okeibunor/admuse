<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Controllers\Traits\ProgramValidation;
use App\Http\Library\CSVImport;
use App\Http\Resources\ProgramCollection;
use App\Http\Resources\ProgramResource;
use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    use ProgramValidation;

    public function all()
    {
        return tc(function () {
            return sendSuccessResponse(
                new ProgramCollection(
                    Program::paginate(10)
                )
            );
        });
    }

    public function create(Request $request)
    {
        return tc(function () use ($request) {
            $this->validateCreate($request);
            /**
             * @var Program $program
             */
            $program = getMediaHouse()->program()
                ->create(
                    array_merge(
                        [
                            'draft' => $this->request->get('draft', false) == 'true',
                            'short_name' => guessShortName($request->get('name'))
                        ],
                        $request->except(array_merge(['id', 'image', 'draft'], Program::$relationshipNames))
                    ));
            $program->saveRelateables($request);
            foreach ($request->get('image') as $image) {
                File::assignToAnotherModel($image, $program->id, Program::class);
            }
            return sendSuccessResponse(new ProgramResource($program));
        });
    }

    public function bulk()
    {
        return tc(
            function () {
                $count = 0;
                $media = getMediaHouse();

                foreach (CSVImport::getEachLine('setProgramIndex', 'programData') as $data) {
                    $media->program()->create(
                        array_merge($data, [
                            'short_name' => guessShortName($data['name']),
                            'channel_id' => 1,
                            'sub_channel_id' => 1,
                            'type_id' => 1,
                            'schedule_id' => 1,
                            'cost_factor_id' => 1,
                            'draft' => false
                        ])
                    );
                    $count++;
                }

                return sendSuccessResponse(['processed' => $count]);
            });
    }

    public function list()
    {
        return sendSuccessResponse(new ProgramCollection(
            Program::getMediaHouseProgramPaginated(10)
        ));
    }

    public function drafts()
    {
        return sendSuccessResponse(new ProgramCollection(
            Program::getMediaHouseDraftProgramPaginated(10)
        ));
    }

    public function view()
    {
        return tc(function () {
            return sendSuccessResponse(
                new ProgramResource(
                    getCurrentProgram()
                )
            );
        });
    }
}
