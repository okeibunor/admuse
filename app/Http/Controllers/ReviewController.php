<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Exception\ValidationException;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Resources\ReviewResource;
use App\Http\Resources\PaginatedResource;

class ReviewController extends Controller
{
    public function index()
    {
        return tc(function(){
            $slot = getRequestedSlot();

            $reviews = $slot->reviews()->paginate(15);

            if($reviews->isEmpty()){
                return sendResponse('error', [
                    'type' => 'No Data'
                ]);
            }

            return sendSuccessResponse([
                'rating' => ceil($slot->reviews()->avg('rating')) . '/5',
                'reviews' => new PaginatedResource($reviews, ReviewResource::class)
            ]);
        });
    }

    public function create()
    {
        return tc(function() {
            $this->validateCreate();

            $slot = \getRequestedSlot();

            $review = $slot->reviews()->where('user_id', $this->user->id)->first();

            if(!$review){
                $review = new Review;
            }
            
            $review->rating = $this->request->get('rating', 1);
            $review->review = $this->request->get('review');
            $review->user_id = $this->user->id;
            $review->slot_id = $slot->id;
            $review->save();
    
            return \sendSuccessResponse(new ReviewResource($review));
        }); 
    }

    public function validateCreate()
    {
        $validator = Validator::make($this->request->all(), [
            'rating' => 'required|int|min:1|max:5',
            'review' => 'min:1|string'
        ]);

        if($validator->fails()) throw new ValidationException($validator);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function edit(Review $review)
    {
        return $this->create();
    }

    public function delete()
    {
        return tc(function(){
            $review = $slot->reviews()->where('user_id', $this->user->id)->first();

            $review->delete();

            return sendSuccessResponse('Slot Review Deleted');
        });
    }
}
