<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Exceptions\ValidationException;
use App\Http\Resources\ContactResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class ContactController extends Controller
{
    public function create()
    {
        return tc(function () {
            $this->validateCreate();
            return sendSuccessResponse(
                new ContactResource(
                    Contact::create(
                        array_merge(
                            $this->request->only(['name', 'mobile']),
                            ['media_id' => getMediaHouse()->id]
                        )
                    )
                )
            );
        });
    }

    /**
     * @throws ValidationException
     */
    public function validateCreate()
    {
        $validator = Validator::make($this->request->all(), [
            'name' => 'required',
            'mobile' => 'required'
        ]);

        $exist = Contact::where('name', $this->request->get('name'))
            ->where('media_id', getMediaHouse()->id)
            ->exists();

        if ($exist) {
            $messageBag = new MessageBag(['name.exists' => 'Contact name already exists']);
            throw new ValidationException($messageBag);
        }
        if ($validator->fails()) throw new ValidationException($validator);
    }

    public function delete()
    {
        return tc(function () {
            $contact = Contact::where('name', $this->request->route('contact_name'))
                ->first();

            if ($contact->media_id !== getMediaHouse()->id) {
                return sendResponse('error', [
                    'type' => 'Access Denied',
                    'message' => 'Contact Invalid'
                ], 401);
            }
            $contact->forceDelete();
            return sendSuccessResponse('Contact Deleted Successfully');
        });
    }

    public function list()
    {
        return tc(function () {
            $contacts = getMediaHouse()->contacts;
            return sendSuccessResponse($contacts->map(function (Contact $contact) {
                return new ContactResource($contact);
            }));
        });
    }

    public function edit()
    {
        return tc(function () {
            $this->validateCreate();
            $contact = Contact::where('name', $this->request->route('contact_name'))->first();


            if ($contact->media_id !== getMediaHouse()->id) {
                return sendResponse('error', [
                    'type' => 'Access Denied',
                    'message' => 'Contact Invalid'
                ], 401);
            }

            $contact->update($this->request->only('name'));

            return sendSuccessResponse('Contact Edited');
        });
    }
}
