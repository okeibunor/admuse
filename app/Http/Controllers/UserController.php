<?php

namespace App\Http\Controllers;

use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\Http\Resources\PaginatedResource;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function profile()
    {
        return tc(function () {
            return sendSuccessResponse(
                new UserResource($this->user)
            );
        });
    }

    public function view()
    {
        return tc(function () {
            return sendSuccessResponse(
                new UserResource(getRequestedUser())
            );
        });
    }

    public function edit()
    {
        return tc(function () {
            $this->validateEdit();

            $user = getRequestedUser();

            $user->update(
                $this->request->only([
                    'phone', 'address', 'business_name',
                    'sector', 'website', 'products_and_services',
                    'name',
                ])
            );

            return sendSuccessResponse('Profile Updated');
        });
    }

    /**
     * @throws ValidationException
     */
    public function validateEdit()
    {
        $validator = Validator::make($this->request->all(), [
            '*' => 'nullable|min:3'
        ]);

        if ($validator->fails()) throw new ValidationException($validator);
    }

    /**
     * Return paginated response for admin containing all users
     * information
     *
     * @return JsonResource
     */
    public function all()
    {
        return tc(function () {
            $users = User::latest()->paginate(20);

            if ($users->isEmpty()) throw new Custom('No Data Available');

            return sendSuccessResponse(
                new PaginatedResource($users, UserResource::class)
            );
        });
    }
}
