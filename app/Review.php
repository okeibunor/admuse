<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
