<?php

namespace App;

use App\Traits\Relateable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static self|Builder where(string $string, $program)
 * @property Channel channel
 * @property RelationShipPath relations
 * @property Type type
 * @property Media media
 * @property Collection|Contact[] contacts
 * @property Collection|Gender[] genders
 * @property Collection|MaritalStatus[] maritalStatus
 * @property Collection|CostFactor[] costFactor
 * @property Collection|ProfessionLevel[] professionLevel
 * @property Collection|ageGroup[] ageGroup
 * @property Collection|Interest[] interest
 * @property Collection|LiteracyLevel[] literacyLevel
 * @property Collection|SocialClass[] socialClass
 * @property Schedule schedule
 * @property SubChannel subChannel
 * @property mixed short_name
 * @property mixed reach_max
 * @property mixed reach_min
 * @property mixed address
 * @property mixed description
 * @property mixed name
 * @property Collection|File[] images
 * @property mixed location
 * @method  Builder draft()
 * @method  Builder notDraft()
 * @method static \Illuminate\Pagination\LengthAwarePaginator paginate(int $int)
 */
class Program extends Model
{
    use Relateable;

    public static $relationshipNames = ['social_class', 'age_group', 'interest', 'literacy_level', 'profession_level', 'contacts', 'genders', 'marital_status'];

    protected $guarded = [];

    /**
     * @param $program
     * @return Program|Builder|Model|object
     */
    public static function getProgram($program)
    {
        return self::where('short_name', $program)->first();
    }

    /**
     * @param int $length
     * @return LengthAwarePaginator
     */
    public static function getMediaHouseProgramPaginated($length = 10)
    {
        return self::where('media_id', getMediaHouse()->id)
            ->notDraft()->paginate($length);
    }

    /**
     * @param int $length
     * @return LengthAwarePaginator
     */
    public static function getMediaHouseDraftProgramPaginated($length = 10)
    {
        return self::where('media_id', getMediaHouse()->id)
            ->draft()->paginate($length);
    }

    public function scopeDraft(Builder $query)
    {
        return $query->where('draft', true);
    }

    public function scopeNotDraft(Builder $query)
    {
        return $query->where('draft', false);
    }

    public function slot()
    {
        return $this->hasMany(Slot::class);
    }

    /**
     * @return BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function subChannel()
    {
        return $this->belongsTo(SubChannel::class);
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function costFactor()
    {
        return $this->belongsTo(CostFactor::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function images()
    {
        return $this->morphMany(File::class, 'model', 'model', 'object_id');
    }
}
