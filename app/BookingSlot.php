<?php

namespace App;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property mixed material
 * @property Slot slot
 * @property mixed booking_id
 * @property Booking booking
 * @property mixed slot_id
 * @property string status
 * @property mixed quantity
 * @property Carbon published_at
 * @property Carbon preferred_publication_date
 * @property mixed media_house_note
 * @method static Builder join(string $table, string $field1, string $operator, string $field2)
 * @method static Builder|self where(string $string, \Illuminate\Routing\Route|object|string $route)
 * @method static self create(array $array)
 * @method static Builder whereRaw(string $string)
 */
class BookingSlot extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
        'preferred_publication_date' => 'datetime',
        'published_at' => 'datetime'
    ];

    /**
     * Get list of booked_slots belonging to a media house
     *
     * @param $mediaHouseId integer
     *
     * @return Builder
     */
    public static function mediaHouseBookedItems(int $mediaHouseId)
    {
        return self::join('slots', 'booking_slots.slot_id', '=', 'slots.id')
            ->join('bookings','bookings.id','=','booking_slots.booking_id')
            ->where('slots.media_id', '=', $mediaHouseId)
            ->where('bookings.paid', '=', true)
            ->select(['booking_slots.*', 'slots.media_id']);
    }

    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }

    public function material()
    {
        return $this->morphMany(File::class, 'model', 'model', 'object_id');
    }

    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}
