<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string status
 * @property mixed amount
 * @property mixed user_id
 * @property mixed paid
 * @property mixed created_at
 * @property mixed updated_at
 * @property BookingSlot[]|Collection bookedItems
 * @property mixed id
 * @property mixed amount_paid
 * @property User user
 * @property mixed transaction_fee
 * @property mixed payment_method
 * @method static Builder|self where(string $string, \Illuminate\Routing\Route|object|string $bookingId)
 */
class Booking extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getItemsSum()
    {
        if(!$this->amount){
            return
                $this->bookedItems()
                    ->join('slots', 'slots.id', '=', 'booking_slots.slot_id')
                    ->sum('price');
        }
        return $this->amount;
    }

    public function bookedItems()
    {
        return $this->hasMany(BookingSlot::class);
    }
}
