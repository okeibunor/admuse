<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class WithdrawalRequest
 * @package App
 *
 * @property User user
 * @property User admin
 * @property User amount
 * @property mixed id
 * @property mixed status
 * @property mixed user_id
 * @property mixed admin_id
 * @property Carbon created_at
 * @property Carbon updated_at
 * @method  static Builder latest()
 * @method static Builder where(string $string, $get)
 */
class WithdrawalRequest extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id');
    }
}
