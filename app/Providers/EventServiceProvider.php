<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            'App\Listeners\SendAccountVerification',
            'App\Listeners\SendWelcomeMail'
        ],
        'App\Events\BookingSaved' => [
            'App\Listeners\NotifyUserOnBookingSaved'
        ],
        'App\Events\BookingItemApproved' => [
            'App\Listeners\NotifyUserOnBookingItemApproval'
        ],
        'App\Events\BookingItemRejected' => [
            'App\Listeners\NotifyUserOnBookingItemRejection'
        ],
        'App\Events\BookingItemMaterialRejected' => [
            'App\Listeners\NotifyUserOnBookingItemMaterialRejection'
        ],
        'App\Events\BookingItemPublished' => [
            'App\Listeners\NotifyUserOnBookingItemPublished'
        ],
        'App\Events\BookingItemScheduled' => [
            'App\Listeners\NotifyUserOnBookingItemSchedule'
        ],
        'App\Events\BookingItemHasNoMaterial' => [
            'App\Listeners\NotifyUserOnBookingItemHasNoMaterial'
        ],
        'App\Events\BookingPaid' => [
            'App\Listeners\NotifyUserOfPayment',
            'App\Listeners\NotifyMediaForBooking',
            'App\Listeners\AddPaymentForAdmin'
        ],
        'App\Events\WalletTransactionEvent' => [
            'App\Listeners\WalletTransactionListener'
        ],
        'App\Events\WalletWithdrawRequest' => [
            'App\Listeners\DebitUserForWithdrawRequest',
            'App\Listeners\NotifyAdminsOnWithdrawRequest'
        ],
        'App\Events\WalletWithdrawRejected' => [
            'App\Listeners\RefundUserOfWithdrawRejection',
            'App\Listeners\NotifyUserOfWithdrawRejection'
        ],
        'App\Events\WalletWithdrawApproval' => [
            'App\Listeners\TransferToUserAccountForWithdraw',
            'App\Listeners\NotifyUserOfWithdrawApproval'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
