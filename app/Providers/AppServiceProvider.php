<?php

namespace App\Providers;

use App\BookingSlot;
use App\File;
use App\Observers\BookingItemObserver;
use App\Observers\FileObserver;
use App\Observers\UserObserver;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        BookingSlot::observe(BookingItemObserver::class);
        File::observe(FileObserver::class);
        User::observe(UserObserver::class);
    }
}
