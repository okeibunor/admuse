<?php

namespace App;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static self|Builder where(string $string, string $operator, string $value = '')
 * @property Program program
 * @property Media media
 * @property mixed price
 * @property mixed name
 * @property string mediaType
 * @property Collection|File[] images
 * @property mixed draft
 * @property bool available
 * @property mixed type_id
 * @property mixed media_type_id
 * @method Builder draft()
 * @method Builder notDraft()
 * @method static LengthAwarePaginator paginate(int $int)
 * @property Size size
 */
class Slot extends Model
{
    protected $guarded = [];

    /**
     * @param int $length
     * @return LengthAwarePaginator
     */
    public static function getProgramPaginatedSlots(int $length)
    {
        return self::where('program_id', getCurrentProgram()->id)
            ->notDraft()
            ->paginate($length);
    }

    /**
     * @param int $length
     * @return LengthAwarePaginator
     */
    public static function getProgramPaginatedDraftSlots(int $length)
    {
        return self::where('program_id', getCurrentProgram()->id)
            ->draft()
            ->paginate($length);
    }

    public function scopeNotDraft(Builder $query)
    {
        return $query->where('draft', false);
    }

    public function scopeDraft(Builder $query)
    {
        return $query->where('draft', true);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function program()
    {
        return $this->belongsTo(Program::class);
    }

    public function mediaType()
    {
        return $this->belongsTo(MediaType::class);
    }

    public function media()
    {
        return $this->belongsTo(Media::class);
    }

    public function images()
    {
        return $this->morphMany(File::class, 'model', 'model', 'object_id');
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
