<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static self create(array $array)
 */
class Token extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public static function createForUser(User $user)
    {
        $token = self::makeToken();
        // save token
        self::create([
            'owner' => $user->id,
            'value' => $token,
            'expire_at' => now()->addMinutes(15)
        ]);
        return $token;
    }

    /**
     * @return float|int
     * @throws \Exception
     */
    public static function makeToken()
    {
        return random_int(4, 100) * time() * time();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'owner');
    }
}
