<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SubChannel implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return \App\SubChannel::where('id', $value)
            ->where('channel_id', request('channel_id'))
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Given sub channel doesn\'t match channel';
    }
}
