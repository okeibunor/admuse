<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * This class is in charge of redirecting every
 * info of Program that defines it.
 * It allows us to assign multiple of the same information.
 *
 * Class RelationShipPath
 * @package App
 */
class RelationShipPath extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
