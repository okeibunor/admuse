<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Builder where(string $string, string $route)
 */
class MaritalStatus extends Model
{
    public $timestamps = false;
    protected $guarded = [];
}
