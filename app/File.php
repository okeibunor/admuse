<?php

namespace App;

use App\Observers\FileObserver;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Builder|self where(string $string, \Illuminate\Routing\Route|object|string $fileId)
 */
class File extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function owner()
    {
        return $this->morphTo('model', 'model', 'object_id');
    }

    public static function assignToAnotherModel(int $id, int $objectId, string $model)
    {
        return self::where('id', $id)->first()
            ->update([
                'object_id' => $objectId,
                'model' => $model
            ]);
    }
}
