<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static self|Builder where(string $string, $interest)
 */
class Interest extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
