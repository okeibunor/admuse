<?php

namespace App\Events;

use App\WithdrawalRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WalletWithdrawRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var WithdrawalRequest
     */
    public $request;

    /**
     * Create a new event instance.
     *
     * @param WithdrawalRequest $request
     */
    public function __construct(WithdrawalRequest $request)
    {
        $this->request = $request;
    }
}
