<?php

namespace App\Events;

use App\BookingSlot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BookingItemScheduled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var BookingSlot
     */
    public $slot;

    /**
     * Create a new event instance.
     *
     * @param BookingSlot $slot
     */
    public function __construct(BookingSlot $slot)
    {
        //
        $this->slot = $slot;
    }
}
