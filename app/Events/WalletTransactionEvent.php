<?php

namespace App\Events;

use App\Wallet;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WalletTransactionEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Wallet
     */
    public $wallet;
    /**
     * @var string
     */
    public $message;
    public $type;
    /**
     * @var float
     */
    public $amount;
    public $reference;

    /**
     * Create a new event instance.
     *
     * @param Wallet $wallet
     * @param float $amount
     * @param $type
     * @param string $message
     * @param null $reference
     */
    public function __construct(Wallet $wallet, float $amount, $type, string $message, $reference = null)
    {
        $this->wallet = $wallet;
        $this->message = $message;
        $this->type = $type;
        $this->amount = $amount;
        $this->reference = $reference;
    }
}
