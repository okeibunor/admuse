<?php

namespace App\Events;

use App\WithdrawalRequest;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WalletWithdrawRejected
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var WithdrawalRequest
     */
    public $withdrawalRequest;

    /**
     * Create a new event instance.
     *
     * @param WithdrawalRequest $withdrawalRequest
     */
    public function __construct(WithdrawalRequest $withdrawalRequest)
    {
        $this->withdrawalRequest = $withdrawalRequest;
    }
}
