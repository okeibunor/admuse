<?php

namespace App\Events;

use App\BookingSlot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BookingItemApproved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var BookingSlot
     */
    public $booking;

    /**
     * Create a new event instance.
     *
     * @param BookingSlot $booking
     */
    public function __construct(BookingSlot $booking)
    {
        $this->booking = $booking;
    }
}
