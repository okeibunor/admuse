<?php

namespace App\Events;

use App\BookingSlot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BookingItemRejected
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var BookingSlot
     */
    public $bookingItem;

    /**
     * Create a new event instance.
     *
     * @param BookingSlot $bookingItem
     */
    public function __construct(BookingSlot $bookingItem)
    {
        $this->bookingItem = $bookingItem;
    }
}
