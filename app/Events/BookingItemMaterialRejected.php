<?php

namespace App\Events;

use App\BookingSlot;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BookingItemMaterialRejected
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var BookingSlot
     */
    public $bookingSlot;

    /**
     * Create a new event instance.
     *
     * @param BookingSlot $bookingSlot
     */
    public function __construct(BookingSlot $bookingSlot)
    {
        $this->bookingSlot = $bookingSlot;
    }
}
