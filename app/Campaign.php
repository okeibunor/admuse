<?php

namespace App;

use App\Traits\Relateable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

/**
 * Class Campaign
 * @package App
 * @property mixed id
 * @property mixed name
 * @property mixed description
 * @property mixed user_id
 * @property mixed location
 * @property mixed budget
 * @property Carbon starts_at
 * @property Carbon ends_at
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Collection<Campaign> $slots
 * @property Collection<Campaign> items
 * @property Collection<Relateable> ageGroup
 * @property Collection<Relateable> socialClass
 * @property Collection<Relateable> interests
 * @property mixed amount
 * @property mixed paid
 * @method static Builder where(string $string, string $campaign_id)
 */
class Campaign extends Model
{
    use Relateable;

    public static $relationshipNames = ['age_group', 'interest', 'social_class'];

    protected $guarded = [];
    protected $casts = [
        'starts_at' => 'date',
        'ends_at' => 'date',
    ];

    public function slots()
    {
        return $this->items();
    }

    public function items()
    {
        return $this->hasMany(CampaignItem::class);
    }

    public function getItemSum()
    {
        if (!$this->amount) {
            return Cache::remember("Campaign:Items:Price:" . $this->id, now()->addDay(7), function () {
                $result = $this->items()
                    ->join('slots', 'slots.id', '=', 'campaign_items.slot_id')
                    ->groupBy(['slots.id', 'quantity', 'price'])
                    ->selectRaw('SUM(slots.price * quantity) total')
                    ->get();
                return $result->sum('total');
            });
        }
        return $this->amount;
    }

    public function hasSlot($id)
    {
        return $this->items()->where('slot_id', $id)->exists();
    }
}
