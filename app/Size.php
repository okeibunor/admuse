<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * @method static LengthAwarePaginator paginate(int $int)
 * @method static self firstOrCreate(array $array)
 * @method static Builder where(string $string, $size)
 */
class Size extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->size,
            'size' => $this->size,
        ];
    }
}
