<?php

namespace App;

use App\Exceptions\Custom;
use App\Traits\UserCreate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 *
 * @method static self create(array $attributes)
 * @method static self|Builder where(string $string, $email)
 * @method static self|Builder latest(string $column = 'id')
 * @property AccountType type
 * @property mixed $name
 * @property mixed id
 * @property mixed email
 * @property mixed password
 * @property Wallet wallet
 * @property BankDetail bankDetail
 * @property Media $publication
 * @property Media media
 * @property mixed phone
 * @property mixed business_name
 * @property mixed sector
 * @property mixed address
 * @property mixed website
 * @property mixed products_and_services
 * @property Collection<Campaign> campaign
 * @package App
 */
class User extends Authenticatable
{
    use UserCreate, Notifiable;

    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(AccountType::class, 'account_type_id', 'id');
    }

    /**
     * @return HasOne|Media
     */
    public function media()
    {
        return $this->hasOne(Media::class);
    }

    public function booking()
    {
        return $this->hasMany(Booking::class);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function isNotAdmin()
    {
        return !$this->isAdmin();
    }

    public function isAdmin()
    {
        return $this->type->id === AccountType::admin()->id;
    }

    public function isNotPublisher()
    {
        return !$this->isPublisher();
    }

    public function isPublisher()
    {
        return $this->type->id === AccountType::publisher()->id;
    }

    public function stringnifiedForPayment($others = null)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'business_name' => $this->business_name,
        ];

        if ($others) $data = array_merge($data, $others);

        return json_encode($data);
    }

    /**
     * @param $paymentAmount
     * @throws Custom
     */
    public function checkUserBalance($paymentAmount)
    {
        if ($paymentAmount > $this->wallet->amount) {
            throw new Custom('Insufficient Fund');
        }
    }

    /**
     * @param $amount
     * @return \Illuminate\Database\Eloquent\Model|WithdrawalRequest
     */
    public function withdraw($amount)
    {
        return $this->withdrawals()->create([
            'amount' => $amount,
            'status' => 'waiting'
        ]);
    }

    public function withdrawals()
    {
        return $this->hasMany(WithdrawalRequest::class);
    }

    public function bankDetail()
    {
        return $this->hasOne(BankDetail::class);
    }

    public function isAffiliate()
    {
        return $this->type->isAffiliate();
    }

    public function isNotAffiliate()
    {
        return $this->type->isNotAffiliate();
    }

    public function campaign()
    {
        return $this->hasMany(Campaign::class);
    }
}
