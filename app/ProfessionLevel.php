<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfessionLevel extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
