<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class UploadLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:links';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symlink from public to storage for our private uploads';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('app'))) {
            // deleting the directory from public because
            // the storage might have been updated
            File::deleteDirectory(public_path('app'));
        }
        // create the directories
        try {
            File::makeDirectory(
                storage_path(config('filesystems.media.upload')),
                0755, true);
        } catch (\ErrorException $e) {
        }

        try {
            File::makeDirectory(
                storage_path(config('filesystems.material.upload')),
                0755, true);
        } catch (\ErrorException $e) {
        }

        File::link(storage_path('app'), public_path('app'));
        $this->info('Directory linked successfully.');
    }
}
