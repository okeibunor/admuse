<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property Slot slot
 * @property mixed quantity
 */
class CampaignItem extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function slot()
    {
        return $this->belongsTo(Slot::class);
    }
}
