<?php

namespace App;

use App\Events\WalletTransactionEvent;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed amount
 */
class Wallet extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $guarded = [];

    public function credit($amount, $message, $reference = null)
    {
        $this->amount = $this->amount + $amount;

        $this->update([
            'amount' => $this->amount
        ]);

        event(new WalletTransactionEvent($this, $amount, "credit", $message, $reference));
        return true;
    }

    public function debit($amount, $message)
    {
        $this->amount = $this->amount - $amount;

        $this->update([
            'amount' => $this->amount
        ]);

        event(new WalletTransactionEvent($this, $amount, "debit", $message));
        return true;
    }
}
