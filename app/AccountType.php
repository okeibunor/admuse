<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $id
 * @property mixed $name
 * @property mixed $description
 * @method static self|Builder firstOrCreate(array $array)
 * @method static self|Builder where(string $string, string $string1)
 */
class AccountType extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public static function advertiser()
    {
        return self::where('name', 'advertiser')->first();
    }

    public static function affiliate(){
        return self::where('name', 'affiliate')->first();
    }

    public static function publisher()
    {
        return self::where('name', 'publisher')->first();
    }

    public static function administrator()
    {
        return self::admin();
    }

    /**
     * @return Builder|self
     */
    public static function admin()
    {
        return self::where('name', 'administrator')->first();
    }

    public function isAffiliate()
    {
        return $this->id === $this->affiliate()->id;
    }

    public function isNotAffiliate()
    {
        return !$this->isAffiliate();
    }

    public function isNotAdmin()
    {
        return !$this->isAdmin();
    }

    public function isAdmin()
    {
        return $this->id === $this::admin()->id;
    }
}
