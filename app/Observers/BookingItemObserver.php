<?php

namespace App\Observers;

use App\BookingSlot;

class BookingItemObserver
{
    /**
     * Handle the booking slot "created" event.
     *
     * @param \App\BookingSlot $bookingSlot
     * @return void
     */
    public function created(BookingSlot $bookingSlot)
    {
        //
    }

    /**
     * Handle the booking slot "updated" event.
     *
     * @param \App\BookingSlot $bookingSlot
     * @return void
     */
    public function updated(BookingSlot $bookingSlot)
    {
        // todo: guess the status for booking
    }

    /**
     * Handle the booking slot "deleted" event.
     *
     * @param \App\BookingSlot $bookingSlot
     * @return void
     */
    public function deleted(BookingSlot $bookingSlot)
    {
    }

    /**
     * Handle the booking slot "restored" event.
     *
     * @param \App\BookingSlot $bookingSlot
     * @return void
     */
    public function restored(BookingSlot $bookingSlot)
    {
        //
    }

    /**
     * Handle the booking slot "force deleted" event.
     *
     * @param \App\BookingSlot $bookingSlot
     * @return void
     */
    public function forceDeleted(BookingSlot $bookingSlot)
    {
        //
    }
}
