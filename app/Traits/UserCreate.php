<?php

namespace App\Traits;

use App\AccountType;

trait UserCreate
{
    /**
     * @param $name
     * @param $email
     * @param $password
     * @param string $type
     * @return self
     */
    public static function createUser($name, $email, $password, $type = 'advertiser')
    {
        return self::create([
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'account_type_id' => AccountType::$type()->id
        ]);
    }
}
