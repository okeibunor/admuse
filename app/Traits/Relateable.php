<?php

namespace App\Traits;

use App\AgeGroup;
use App\Contact;
use App\Gender;
use App\Interest;
use App\LiteracyLevel;
use App\MaritalStatus;
use App\ProfessionLevel;
use App\Program;
use App\RelationShipPath;
use App\SocialClass;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

trait Relateable
{
//    public static $relationshipNames = [];

    public function saveRelateables(Request $request)
    {
        return $this->relations()->createMany($this::makeRelateables($request));
    }

    /**
     * @return HasMany
     */
    private function relations()
    {
        return $this->hasMany(RelationShipPath::class, 'parent_id', 'id')
            ->where('parent_type', self::class);
    }

    public static function makeRelateables(Request $request)
    {
        $relationData = [];
        collect($request->only(self::$relationshipNames))
            ->map(function (array $relationValue, string $relationName)
            use (&$relationData) {
                switch ($relationName) {
                    case 'social_class':
                        Program::makeRelateableField(SocialClass::class, $relationValue, $relationData);
                        break;
                    case 'marital_status':
                        Program::makeRelateableField(MaritalStatus::class, $relationValue, $relationData);
                        break;
                    case 'contacts':
                        Program::makeRelateableField(Contact::class, $relationValue, $relationData);
                        break;
                    case 'genders':
                        Program::makeRelateableField(Gender::class, $relationValue, $relationData);
                        break;
                    case 'age_group':
                        Program::makeRelateableField(AgeGroup::class, $relationValue, $relationData);
                        break;
                    case 'interest':
                        Program::makeRelateableField(Interest::class, $relationValue, $relationData);
                        break;
                    case 'literacy_level':
                        Program::makeRelateableField(LiteracyLevel::class, $relationValue, $relationData);
                        break;
                    case 'profession_level':
                        Program::makeRelateableField(ProfessionLevel::class, $relationValue, $relationData);
                        break;
                }
            });
        return $relationData;
    }

    public static function makeRelateableField($name, array $values, &$old)
    {
        foreach ($values as $value) {
            $old[] = [
                'relateable_type' => $name,
                'relateable_id' => $value
            ];
        }
    }

    public function socialClass()
    {
        return $this->getByRelation('social_classes', SocialClass::class);
    }

    public function getByRelation(string $table, string $model)
    {
        return $this->relations()
            ->join($table, 'relateable_id', '=', $table . '.id')
            ->where('relateable_type', $model);
    }

    public function literacyLevel()
    {
        return $this->getByRelation('literacy_levels', LiteracyLevel::class);
    }

    public function interest()
    {
        return $this->getByRelation('interests', Interest::class);
    }

    public function ageGroup()
    {
        return $this->getByRelation('age_groups', AgeGroup::class);
    }

    public function maritalStatus()
    {
        return $this->getByRelation('marital_statuses', MaritalStatus::class);
    }

    public function professionLevel()
    {
        return $this->getByRelation('profession_levels', ProfessionLevel::class);
    }

    public function contacts()
    {
        return $this->getByRelation('contact', Contact::class);
    }

    public function genders()
    {
        return $this->getByRelation('genders', Gender::class);
    }
}
