<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static self|Builder firstOrCreate(array $array)
 * @method static self|Builder join(string $string, string $string1, string $string2, string $string3)
 * @method static self|Builder where(string $string, $value)
 */
class SubChannel extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
