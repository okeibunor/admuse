<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static Builder where(string $string, string $string1)
 */
class MediaType extends Model
{
    public $timestamps = false;
    protected $guarded = [];

    public static function text()
    {
        return self::where('name', 'text')->first()->id;
    }

    public static function image()
    {
        return self::where('name', 'image')->first()->id;
    }

    public static function video()
    {
        return self::where('name', 'video')->first()->id;
    }

    public static function audio()
    {
        return self::where('name', 'audio')->first()->id;
    }

    public static function other()
    {
        return self::where('name', 'other')->first()->id;
    }
}
