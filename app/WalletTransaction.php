<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @method static self|Builder create(array $array)
 * @method static Builder where(string $string, $id)
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property mixed id
 * @property mixed amount
 * @property mixed type
 * @property mixed note
 * @property mixed user_id
 * @property User user
 */
class WalletTransaction extends Model
{
    protected $guarded = [];
    public $incrementing = false;

    public static function getByReference($ref)
    {
        return self::where('reference', $ref)->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
