<?php

use App\Booking;
use App\BookingSlot;
use App\Campaign;
use App\Exceptions\Custom;
use App\Exceptions\ValidationException;
use App\File;
use App\Http\Controllers\FileController;
use App\Media;
use App\Program;
use App\Slot;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

if (!function_exists("tokenStatus")) {
    /**
     * @param string|boolean $token
     * @return string|User
     */
    function tokenStatus($token = false)
    {
        /**
         * Storing the users variable so it will be globally accessible
         */
        global $user;
        global $tokenKey;

        /**
         * Get token from header first
         */
        if (($tokenFromHeader = request()->header('Authorization'))
            && !$token) {
            $token = str_replace('Bearer ', "", $tokenFromHeader);
        }

        /**
         * Saving token inside a variable
         */
        $tokenKey = $token;
        try {
            return ($user = JWTAuth::authenticate($token)) ? $user : 'user_not_found';
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return 'token_expired';
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return 'token_invalid';
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return 'token_absent';
        }
    }
}

if (!function_exists('tc')) {
    /**
     * A helper function for easy try catch usage
     *
     * @param Closure
     * @return mixed
     */
    function tc(Closure $closure)
    {
        /**
         * We want to see errors if we are debugging else, we will try to catch them
         */
        if (env('APP_DEBUG', false)) {
            return call_user_func($closure);
        }

        try {
            return call_user_func($closure);
        } catch (ModelNotFoundException $exception) {
            return sendResponse('error', [
                // 'message' => $exception->getMessage()
            ]);
        } catch (Custom $exception) {
            return sendResponse('error', [
                'message' => $exception->getMessage()
            ]);
        } catch (ValidationException $exception) {
            return sendResponse('error', [
                'message' => $exception->getMessage(),
                'errors' => $exception->getErrors()
            ]);
        } catch (\Illuminate\Validation\ValidationException $exception) {
            return sendResponse('error', [
                'message' => $exception->getMessage(),
                'errors' => $exception->errors()
            ]);
        } catch (Throwable $exception) {
            return sendResponse('error', [
                'message' => $exception->getMessage()
            ]);
        } catch (Exception $exception) {
            return sendResponse('error', [
                'message' => 'Something went wrong!',
                'reason' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ]);
        }
    }
}

if (!function_exists("sendResponse")) {
    /**
     * Provide API Response to the user as JSON object
     * @param string $statusText The status text of the returned response
     *         success - When the expected response is the response provided
     *         error   - When the expected response is not the response given back
     * @param array $data The data to be sent to the user
     * @param int $statusCode The status code for the sent response
     * @return Illuminate\Http\JsonResponse
     */
    function sendResponse($statusText = "error", $data = [], $statusCode = 200)
    {
        return response()
            ->json([
                "statusText" => $statusText,
                "result" => $data,
                "statusCode" => $statusCode
            ])
            ->header("Access-Control-Allow-Origin", "*")
            ->setStatusCode($statusCode);
    }
}

if (!function_exists("sendSuccessResponse")) {
    /**
     * Provide API Response to the user as JSON object
     *
     * @param array $data The data to be sent to the user
     * @return Illuminate\Http\JsonResponse
     */
    function sendSuccessResponse($data = [])
    {
        return sendResponse("success", $data);
    }
}

if (!function_exists('sendNoDataResponse')) {
    function sendNoDataResponse()
    {
        return sendResponse('error', [
            'type' => 'No Date',
            'message' => "No Data!"
        ]);
    }
}

if (!function_exists('guessShortName')) {
    /**
     * @param null $string
     * @return string
     * @throws Exception
     */
    function guessShortName($string = null)
    {

        $alphabets = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z a b c d e f g h i j i k l m n o p q r s t u v w x y z";
        $allowedAlphabets = explode(" ", $alphabets);
        //convert string into a slug
        $string = Str::slug($string);

        $shortName = "";
        $maxPrefixString = strlen($string) <= 5 ? strlen($string) - 1 : 4;
        for ($i = 0; $i <= $maxPrefixString; $i++) {
            if (in_array($string[$i], $allowedAlphabets)) $shortName .= $string[$i];
        }

        return $shortName . rand(50, 3000);
    }
}

if (!function_exists('getMediaHouse')) {
    /**
     * @return Media;
     */
    function getMediaHouse()
    {
        return $GLOBALS['mediaHouse'];
    }
}

if (!function_exists('getUser')) {
    /**
     * @return User;
     */
    function getUser()
    {
        return $GLOBALS['user'];
    }
}

if (!function_exists('getCurrentProgram')) {
    /**
     * @return Program;
     */
    function getCurrentProgram()
    {
        return $GLOBALS['activeProgram'];
    }
}

if (!function_exists('nullable')) {
    function nullable($object, $property)
    {
        try {
            dd($object, property_exists($object, $property));
            return $object->$property();
        } catch (FatalThrowableError $exception) {
            return null;
        }
    }
}

/**
 * Get the current booking which is being access through uri
 *
 * @return Booking|null
 */
function getCurrentBooking()
{
    return @$GLOBALS['currentBooking'];
}

/**
 * Get the current booking item by checking the route
 *
 * @return BookingSlot|null
 */
function getRequestedBookingItem()
{
    return @$GLOBALS['currentBookingItem'];
}

/**
 * @return null|Slot
 */
function getRequestedSlot()
{
    return @$GLOBALS['requestedSlot'];
}

/**
 * @return File|null
 */
function getRequestedFile()
{
    return @$GLOBALS['requestedFile'];
}

/**
 * @return User|null
 */
function getRequestedUser()
{
    return @$GLOBALS['requestedUser'];
}

/**
 * @throws ValidationException
 */
function generalValidation()
{
    $validator = Validator::make(request()->all(), [
        'name' => 'required|min:3'
    ]);

    if ($validator->fails()) throw new ValidationException($validator);
}

/**
 * @param UploadedFile $file
 * @param null $rename
 * @return \Symfony\Component\HttpFoundation\File\File
 */
function moveMediaUpload(UploadedFile $file, $rename = null)
{
    if ($rename) {
        $name = $rename . '.' . $file->clientExtension();
    }

    return $file->move(
        storage_path(
            getMediaAvatarUploadPath()
        ), @$name
    );
}

function getMediaAvatarUploadPath()
{
    return config('filesystems.media.upload');
}

function getProgramUploadPath()
{
    return config('filesystems.program.upload');
}

/**
 * @param UploadedFile $file
 * @return File
 */
function uploadImage(UploadedFile $file)
{
    return FileController::upload($file);
}

/**
 * @return null|Campaign
 */
function getRequestedCampaign()
{
    return @$GLOBALS['requestedCampaign'];
}

/**
 * @return null|\App\CampaignItem
 */
function getRequestedCampaignItem()
{
    return @$GLOBALS['requestedCampaignItem'];
}
