<?php

namespace App\Listeners;

use App\Events\WalletWithdrawRejected;
use App\Notifications\WithdrawRejected;

class NotifyUserOfWithdrawRejection
{
    /**
     * Send Mail Notification
     *
     * @param WalletWithdrawRejected $event
     * @return void
     */
    public function handle(WalletWithdrawRejected $event)
    {
        $event->withdrawalRequest->user
            ->notify(new WithdrawRejected($event->withdrawalRequest));
    }
}
