<?php

namespace App\Listeners;

use App\Events\BookingPaid;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddPaymentForAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingPaid  $event
     * @return void
     */
    public function handle(BookingPaid $event)
    {
        //
    }
}
