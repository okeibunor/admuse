<?php

namespace App\Listeners;

use App\BookingSlot;
use App\Events\BookingPaid;
use App\Notifications\Publisher\NewBooking;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class NotifyMediaForBooking
{
    /**
     * Handle the event.
     *
     * @param BookingPaid $event
     * @return void
     */
    public function handle(BookingPaid $event)
    {
        collect($event->booking->bookedItems)
            ->each(function (BookingSlot $item) {
                Notification::route('mail', $item->slot->media->email)
                    ->notify(
                        new NewBooking($item)
                    );
            });
    }
}
