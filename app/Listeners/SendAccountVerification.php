<?php

namespace App\Listeners;

use App\Notifications\AccountConfirmation;
use App\Token;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SendAccountVerification
{
    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     * @throws \Exception
     */
    public function handle(Registered $event)
    {
        /**
         * @var User user
         */
        $user = $event->user;
        $token = Token::createForUser($user);
        // create token and save
        $user->notify(new AccountConfirmation($token));
    }
}
