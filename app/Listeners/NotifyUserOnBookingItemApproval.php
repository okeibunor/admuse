<?php

namespace App\Listeners;

use App\Events\BookingItemApproved;
use App\Notifications\BookingItemApproved as Notification;

class NotifyUserOnBookingItemApproval
{

    /**
     * Handle the event.
     *
     * @param BookingItemApproved $event
     * @return void
     */
    public function handle(BookingItemApproved $event)
    {
        $event->booking->booking->user->notify(new Notification($event->booking));
    }
}
