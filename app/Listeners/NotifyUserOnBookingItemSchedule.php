<?php

namespace App\Listeners;

use App\Events\BookingItemScheduled;
use App\Notifications\BookingItemScheduled as Notification;

class NotifyUserOnBookingItemSchedule
{
    /**
     * Handle the event.
     *
     * @param BookingItemScheduled $event
     * @return void
     */
    public function handle(BookingItemScheduled $event)
    {
        $event->slot->booking->user->notify(new Notification($event->slot));
    }
}
