<?php

namespace App\Listeners;

use App\Events\WalletWithdrawRequest;

class DebitUserForWithdrawRequest
{
    /**
     * Handle the event.
     *
     * @param WalletWithdrawRequest $event
     * @return void
     */
    public function handle(WalletWithdrawRequest $event)
    {
        $event->request->user->wallet
            ->debit($event->request->amount, 'Withdrawal Request');
    }
}
