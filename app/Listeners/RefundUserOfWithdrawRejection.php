<?php

namespace App\Listeners;

use App\Events\WalletWithdrawRejected;

class RefundUserOfWithdrawRejection
{
    /**
     * Refund the User
     *
     * @param WalletWithdrawRejected $event
     * @return void
     */
    public function handle(WalletWithdrawRejected $event)
    {
        $event->withdrawalRequest->user
            ->wallet->credit(
                $event->withdrawalRequest->amount,
                'Withdraw Rejected'
            );
    }
}
