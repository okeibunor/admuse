<?php

namespace App\Listeners;

use App\Events\BookingItemRejected;
use App\Notifications\BookingItemRejected as Notification;

class NotifyUserOnBookingItemRejection
{
    /**
     * Handle the event.
     *
     * @param BookingItemRejected $event
     * @return void
     */
    public function handle(BookingItemRejected $event)
    {
        $event->bookingItem->booking->user->notify(new Notification($event->bookingItem));
    }
}
