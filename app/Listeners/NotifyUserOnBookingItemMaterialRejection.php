<?php

namespace App\Listeners;

use App\Events\BookingItemMaterialRejected;
use App\Notifications\BookingItemMaterialRejected as Notification;

class NotifyUserOnBookingItemMaterialRejection
{
    /**
     * Handle the event.
     *
     * @param BookingItemMaterialRejected $event
     * @return void
     */
    public function handle(BookingItemMaterialRejected $event)
    {
        $event->bookingSlot->booking->user->notify(new Notification($event->bookingSlot));
    }
}
