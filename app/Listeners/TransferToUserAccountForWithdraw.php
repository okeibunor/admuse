<?php

namespace App\Listeners;

use App\Events\WalletWithdrawApproval;
use App\Exceptions\Custom;
use App\Http\Library\PayStack;
use GuzzleHttp\Exception\GuzzleException;

class TransferToUserAccountForWithdraw
{
    /**
     * Transfer To Bank Account
     *
     * @param WalletWithdrawApproval $event
     * @return void
     * @throws Custom
     * @throws GuzzleException
     */
    public function handle(WalletWithdrawApproval $event)
    {
        PayStack::transfer(
            $event->withdrawalRequest->amount * 100,
            $event->withdrawalRequest->user
        );
    }
}
