<?php

namespace App\Listeners;

use App\Events\WalletTransactionEvent;
use App\Notifications\WalletTransactionNotification;
use App\WalletTransaction;

class WalletTransactionListener
{
    /**
     * Create a transaction data
     *
     * @param WalletTransactionEvent $event
     * @return void
     */
    public function handle(WalletTransactionEvent $event)
    {
        // generate id
        $dateTime = str_replace([' ', ':'], '', now()->toDateTimeString());
        $typePrefix = $event->type == 'debit' ? 'DEB' : 'CRE';
        $id = $typePrefix . $dateTime;

        getUser()->notify(new WalletTransactionNotification(
            WalletTransaction::create([
            'id' => $id,
            'user_id' => $event->wallet->user_id,
            'amount' => $event->amount,
            'type' => $event->type,
            'note' => @$event->message,
            'reference' => @$event->reference
        ])));
    }
}
