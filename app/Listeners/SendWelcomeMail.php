<?php

namespace App\Listeners;

use App\Notifications\Welcome;
use Illuminate\Auth\Events\Registered;

class SendWelcomeMail
{
    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $event->user->notify(new Welcome($event->user));
    }
}
