<?php

namespace App\Listeners;

use App\Events\BookingItemHasNoMaterial;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserOnBookingItemHasNoMaterial
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingItemHasNoMaterial  $event
     * @return void
     */
    public function handle(BookingItemHasNoMaterial $event)
    {
        //
    }
}
