<?php

namespace App\Listeners;

use App\Events\WalletWithdrawRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminsOnWithdrawRequest
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WalletWithdrawRequest  $event
     * @return void
     */
    public function handle(WalletWithdrawRequest $event)
    {
        //
    }
}
