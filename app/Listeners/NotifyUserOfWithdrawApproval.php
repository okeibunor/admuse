<?php

namespace App\Listeners;

use App\Events\WalletWithdrawApproval;
use App\Notifications\WithdrawApproved;

class NotifyUserOfWithdrawApproval
{
    /**
     * Handle the event.
     *
     * @param WalletWithdrawApproval $event
     * @return void
     */
    public function handle(WalletWithdrawApproval $event)
    {
        $event->withdrawalRequest->user
            ->notify(new WithdrawApproved($event->withdrawalRequest));
    }
}
