<?php

namespace App\Listeners;

use App\Events\BookingItemPublished;
use App\Notifications\BookingItemPublished as Notification;

class NotifyUserOnBookingItemPublished
{
    /**
     * Handle the event.
     *
     * @param BookingItemPublished $event
     * @return void
     */
    public function handle(BookingItemPublished $event)
    {
        $event->slot->booking->user->notify(new Notification($event->slot));
    }
}
