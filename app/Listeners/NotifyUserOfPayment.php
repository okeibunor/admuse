<?php

namespace App\Listeners;

use App\Events\BookingPaid;
use App\Notifications\PaymentReceived;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserOfPayment
{
    /**
     * Handle the event.
     *
     * @param  BookingPaid  $event
     * @return void
     */
    public function handle(BookingPaid $event)
    {
        $event->booking->user->notify(
            new PaymentReceived($event->booking)
        );
    }
}
