<?php

namespace App\Listeners;

use App\Events\BookingItemRejected;

class RefundUser
{
    /**
     * Get the amount of the slot and then add it
     * to the wallet of the user who bought it
     *
     * @param BookingItemRejected $event
     * @return void
     */
    public function handle(BookingItemRejected $event)
    {
        $buyer = $event->bookingItem->booking->user;
        $amount = $event->bookingItem->slot->price;
        $mediaHouse = $event->bookingItem->slot->media;

        $buyer->wallet->credit(
            $amount,
            ucfirst($mediaHouse->name) . ' refunded you for rejection of your order.');

        $mediaHouse->user->wallet->debit(
            $amount,
            "Refund for order ({$event->bookingItem->id}) rejection"
        );
    }
}
