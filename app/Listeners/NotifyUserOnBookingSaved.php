<?php

namespace App\Listeners;

use App\Events\BookingSaved;
use App\Notifications\BookingSaved as BookingSavedNotification;

class NotifyUserOnBookingSaved
{

    /**
     * Handle the event.
     *
     * @param BookingSaved $event
     * @return void
     */
    public function handle(BookingSaved $event)
    {
        $event->booking->user->notify(new BookingSavedNotification($event->booking));
    }
}
